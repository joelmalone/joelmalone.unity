﻿using UnityEditor;
using UnityEngine;

namespace JoelMalone.Unity.Editor
{

    [CustomEditor(typeof(MonoBehaviour), true)]
    public class DebugInfoInspector : UnityEditor.Editor
    {

        private bool _expanded = true;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            IDebugTextSource debugInfo = target as IDebugTextSource;
            if (debugInfo != null)
            {

                EditorGUILayout.Separator();
                _expanded = EditorGUILayout.Foldout(_expanded, "Debug Info:");
                if (_expanded)
                {
                    var text = debugInfo.GetDebugText();
                    var content = new GUIContent(text);
                    var size = GUI.skin.label.CalcSize(content);
                    EditorGUILayout.SelectableLabel(text, GUILayout.Height(size.y));
                }
            }
        }

    }

}
