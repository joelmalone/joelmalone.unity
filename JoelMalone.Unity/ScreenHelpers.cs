﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{
    public static class ScreenHelpers
    {

        /// <summary>
        /// Returns Screen.dpi if it seems legit, otherwise guesses the
        /// DPI based on a 21 inch screen.
        /// </summary>
        public static float DpiSafe
        {
            get
            {
                if (Screen.dpi > 0)
                    return Screen.dpi;

                var diagPixels = Mathf.Sqrt(
                    Screen.width * Screen.width +
                    Screen.height * Screen.height);
                var assumedDiagInches = 21;
                var result = diagPixels / assumedDiagInches;
                return result;
            }
        }
    
    }
}
