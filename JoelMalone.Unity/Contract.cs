﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity
{

    /// <summary>
    /// Helper extensions methodsw for verifying things.
    /// </summary>
    public static class ContractExtensions
    {

        public static void AssertNotNull<T>(this T me)
        {
            /**
             * Note the method to this signature used to have a "where T:class" constraint,
             * however, that caused the following runtime exception when running as web player:
             *   VerificationException: Error verifying JoelMalone.Unity.ComponentExtensions:Position<T> (T,UnityEngine.Vector3): Invalid generic method instantiation of method JoelMalone.Core.ContractExtensions::AssertNotNull (generic args don't respect target's constraints) at 0x0057
             *   Respawner.Unity.SpawnpointComponent+<WaitAndRespawnC>d__0.MoveNext ()
             * While not fixing it properly, I have worked around the problem by 
             * removing the constraint.
             **/

            if (me == null)
                throw new AssertNotNullException<T>();
        }

        public class AssertNotNullException<T> : Exception
        {
        }

    }

}
