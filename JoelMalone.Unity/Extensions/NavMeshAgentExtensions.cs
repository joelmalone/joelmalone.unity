﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.UnityPatterns;
using System.Collections;
using JoelMalone.Logging;

namespace JoelMalone.Unity
{

    public static class NavMeshAgentExtensions
    {

        public static IEnumerator NavigateToC(this NavMeshAgent me, Vector3 destination)
        {
            if(!me.SetDestination(destination))
            {
                me.W("Unable to find path.  See scene view for hideous red line of failure.");
                Debug.DrawLine(me.transform.position, destination, Color.red, 60, false);
                yield break;
            }

            while(me.pathPending)
                yield return null;

            while (me.remainingDistance > me.stoppingDistance)
                yield return null;
        }

    }

}
