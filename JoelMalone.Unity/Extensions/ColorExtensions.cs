﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public static class ColorExtensions
    {

        public static Color WithAlpha(this Color me, float alpha)
        {
            return new Color(me.r, me.g, me.b, alpha);
        }

        public static IEnumerator BlendToC(this Color from, Color to, float duration, Action<Color> setter)
        {
            float start = Time.time;
            float end = start + duration;
            while (Time.time < end)
            {
                var elapsed = Time.time - start;
                var t = elapsed / duration;
                var colour = Color.Lerp(from, to, t);
                setter(colour);

                yield return null;
            }

            setter(to);
        }

    }

}