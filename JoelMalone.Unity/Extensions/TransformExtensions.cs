﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.UnityPatterns;
using System.Collections;

namespace JoelMalone.Unity
{

    public static class TransformExtensions
    {

        public static void DestroyAllChildren(this Transform me)
        {
            if (me == null)
                throw new ArgumentNullException();

            while (me.childCount > 0)
            {
                var child = me.GetChild(0);
                child.DestroyGameObject();
            }
        }

        public static IEnumerator MoveAndRotateToC(this Transform me, Transform target, float duration, Easer ease)
        {
            float elapsed = 0;
            var startPosition = me.position;
            var startRotation = me.rotation;
            while (elapsed < duration)
            {
                elapsed = Mathf.MoveTowards(elapsed, duration, Time.deltaTime);
                me.position = Vector3.Lerp(startPosition, target.position, ease(elapsed / duration));
                me.rotation = Quaternion.Lerp(startRotation, target.rotation, ease(elapsed / duration));
                yield return null;
            }
            me.position = target.position;
            me.rotation = target.rotation;
        }

        public static IEnumerable<Transform> EnumerateAncestorsIncludingSelf(this Transform me)
        {
            var transform = me;
            while (transform)
            {
                yield return transform;
                transform = transform.parent;
            }
        }

        /// <summary>
        /// Enumerate the given transform and its descendants in arbitrary order.
        /// </summary>
        /// <param name="me"></param>
        /// <returns></returns>
        public static IEnumerable<Transform> EnumerateDescendantsIncludingSelf(this Transform me)
        {
            yield return me;
            for (int i = 0; i < me.transform.childCount; i++)
            {
                var child = me.GetChild(i);
                foreach (var childResult in EnumerateDescendantsIncludingSelf(child))
                    yield return childResult;
            }
        }

    }

}
