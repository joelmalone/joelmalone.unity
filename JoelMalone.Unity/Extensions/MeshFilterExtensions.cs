﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.Components;

namespace JoelMalone.Unity
{

    public static class MeshFilterExtensions
    {
        
        public static void SetMeshColours(this MeshFilter me, Color colour)
        {
            var colours = new Color[me.mesh.colors.Length];
            colours.SetAll(colour);
            me.mesh.colors = colours;
        }

    }

}