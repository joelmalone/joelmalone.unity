﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.Components;

namespace JoelMalone.Unity
{

    public static class CollectionExtensions
    {

        public static T PickRandom<T>(this IList<T> me, double random0n1)
        {
            if (me == null)
                throw new ArgumentNullException();
            if (me.Count == 0)
                throw new ArgumentException("Given array has no items.");

            var i = (int)Math.Floor(me.Count * random0n1);
            return me[i];
        }

        public static T PickRandom<T>(this T[] me, double random0n1)
        {
            if (me == null)
                throw new ArgumentNullException();
            if (me.Length == 0)
                throw new ArgumentException("Given array has no items.");

            var i = (int)Math.Floor(me.Length * random0n1);
            return me[i];
        }

        public static IEnumerable<T> Reduce<T>(this IEnumerable<T> me, int keepEveryNth)
        {
            var counter = 0;
            foreach(var i in me)
            {
                if (counter++ % keepEveryNth == 0)
                {
                    yield return i;
                }
            }
        }

    }

}