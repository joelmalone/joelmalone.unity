﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.Components;
using JoelMalone.Logging;

namespace JoelMalone.Unity
{

    public static class AudioSourceExtensions
    {
        
        private static System.Random _defaultRandom = new System.Random();

        public static void PlayOneShot(this AudioSource me, IList<AudioClip> clips, System.Random random = null)
        {
            if (clips.Count == 0)
            {
                me.W("Can not play sound - there are no clips defined.");
                return;
            }

            random = random ?? _defaultRandom;
            var r = random.NextDouble();
            var clip = clips.PickRandom(r);
            
            me.PlayOneShot(clip);
        }

    }

}