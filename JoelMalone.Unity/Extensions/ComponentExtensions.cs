﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.UnityPatterns;
using JoelMalone.Unity.Components;

namespace JoelMalone.Unity
{
    public static class ComponentExtensions
    {

        public static T GetComponentOrThrow<T>(this Component me) where T : Component
        {
            me.AssertNotNull();

            var component = me.GetComponent<T>();
            if (component == null)
                throw new InvalidOperationException(string.Format("Component {0} is missing required component {1}.", me, typeof(T).Name));
            return component;
        }

        public static T GetComponentInAncestorsOrThrow<T>(this Component me) where T : Component
        {
            me.AssertNotNull();

            var c = me.GetComponent<T>();
            if (c)
                return c;
            else if (me.transform.parent == null)
                throw new InvalidOperationException(string.Format("Component of type {0} not found in parents.", typeof(T).Name));
            else
                return GetComponentInAncestorsOrThrow<T>(me.transform.parent);
        }

        /// <summary>
        /// Returns all instances of the specified component, searching the immediate 
        /// children of the parent specified.  Unlike the standard GetComponentInChildren(), 
        /// this method does not include the parent object itself.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="me"></param>
        /// <returns></returns>
        public static IEnumerable<T> GetComponentsInChildrenOnly<T>(this Component me) where T : Component
        {
            me.AssertNotNull();

            var matches = from child in me.GetChildGameObjects()
                          let match = child.GetComponent<T>()
                          where match != null
                          select match;
            return matches;
        }

        /// <summary>
        /// Returns the first instance of the specified component, searching the immediate 
        /// children of the parent specified.  Unlike the standard GetComponentInChildren(), 
        /// this method does not include the parent object itself.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="me"></param>
        /// <returns></returns>
        public static T GetComponentInChildrenOnly<T>(this Component me) where T : Component
        {
            me.AssertNotNull();

            var matches = from child in me.GetChildGameObjects()
                          let match = child.GetComponent<T>()
                          where match != null
                          select match;
            var result = matches.FirstOrDefault();
            return result;
        }

        public static T GetComponentAllowingRedirection<T>(this Component me)
            where T : Component
        {
            if (me == null)
                return null;

            var result = me.GetComponent<T>();

            // If no result from the clicked object, check if it has a RedirectorComponent
            if (!result)
            {
                // Although I don't spot any closures, this monstrosity probably results 
                //  in a few allocations from iterators; may be worth investimagating
                result = me.GetComponents<RedirectorComponent>()
                    .Select(c => c.BaseObject)
                    .Where(baseObject => baseObject != null)
                    .Select(baseObject => baseObject.GetComponent<T>())
                    .Where(c => c != null)
                    .FirstOrDefault();
            }

            return result;
        }

        public static T Instantiate<T>(this T me) where T : Component
        {
            me.AssertNotNull();

            return GameObject.Instantiate(me) as T;
        }

        public static T Instantiate<T>(this T me, Vector3 position, Quaternion rotation) where T : Component
        {
            me.AssertNotNull();

            return GameObject.Instantiate(me, position, rotation) as T;
        }

        public static T InstantiateNetworkOrLocal<T>(this T me, Vector3 position, Quaternion rotation) where T : Component
        {
            me.AssertNotNull();

            // Note: I had to rebuild this method from memory, so am not sure
            //  if it'll work!
            if (Network.connections.Length > 0)
                return Network.Instantiate(me, position, rotation, 0) as T;
            else
                return GameObject.Instantiate(me) as T;
        }

        public static void DestroyGameObject(this Component me)
        {
            me.AssertNotNull();

            GameObject.Destroy(me.gameObject);
        }

        public static void DestroyAllChildGameObjects(this Component me)
        {
            me.AssertNotNull();

            me.transform.DestroyAllChildren();
        }

        public static void RecycleAllChildGameObjects(this Component me)
        {
            me.AssertNotNull();

            var transform = me.transform;
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                child.Recycle();
            }
        }

        public static void RecycleAll<T>(this IEnumerable<T> me) where T : Component
        {
            me.AssertNotNull();

            foreach (var i in me)
                i.Recycle();
        }

        public static void RecycleAllAndClear<T>(this ICollection<T> me) where T : Component
        {
            me.AssertNotNull();

            foreach (var i in me)
                i.Recycle();

            me.Clear();
        }

        public static T Position<T>(this T me, Vector3 position) where T : Component
        {
            me.AssertNotNull();

            me.transform.position = position;
            return me;
        }

        public static T LocalPosition<T>(this T me, Vector3 position) where T : Component
        {
            me.AssertNotNull();

            me.transform.localPosition = position;
            return me;
        }

        public static T Rotation<T>(this T me, Quaternion rotation) where T : Component
        {
            me.AssertNotNull();

            me.transform.rotation = rotation;
            return me;
        }

        public static T Parent<T>(this T me, Transform parent) where T : Component
        {
            me.AssertNotNull();

            me.transform.SetParent(parent, true);
            return me;
        }

        public static T Parent<T>(this T me, Component parent) where T : Component
        {
            me.AssertNotNull();
            parent.AssertNotNull();

            me.transform.SetParent(parent.transform, true);
            return me;
        }

        public static T ParentAndReset<T>(this T me, Transform parent) where T : Component
        {
            me.AssertNotNull();

            me.transform.parent = parent;
            me.transform.localPosition = Vector3.zero;
            me.transform.localRotation = Quaternion.identity;
            return me;
        }

        public static T ParentAndReset<T>(this T me, Component parent) where T : Component
        {
            me.AssertNotNull();
            parent.AssertNotNull();

            me.transform.parent = parent.transform;
            me.transform.localPosition = Vector3.zero;
            me.transform.localRotation = Quaternion.identity;
            return me;
        }

        /// <summary>
        /// Sets the parent, but preserves the transform.localPosition value.  Useful for times 
        /// where you need to set transform.localPosition before transform.parent (which alters
        /// localPosition).  As such, this will likely move the transform's world position.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="me"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static T ParentAndPreserveLocalPosition<T>(this T me, Transform parent) where T : Component
        {
            me.AssertNotNull();

            me.transform.SetParent(parent, false);

            return me;
        }

        public static T Name<T>(this T me, string name) where T : Component
        {
            me.AssertNotNull();

            me.name = name;
            return me;
        }

        public static IEnumerable<GameObject> GetChildGameObjects(this Component me)
        {
            var transform = me.transform;
            for (int i = 0; i < transform.childCount; i++)
            {
                yield return transform.GetChild(i).gameObject;
            }
        }

        public static Transform FindOrCreateChild(this Component me, string name)
        {
            var existing = me.transform.FindChild(name);
            if (!existing)
            {
                existing = new GameObject(name)
                .transform
                .ParentAndReset(me);
            }
            return existing;
        }

        public static T GetComponentInDescendantsIncludingSelf<T>(this Component me) where T : Component
        {
            return GetComponentsInDescendantsIncludingSelf<T>(me)
                .FirstOrDefault();
        }

        public static IEnumerable<T> GetComponentsInDescendantsIncludingSelf<T>(this Component me) where T : Component
        {
            return me.transform.EnumerateDescendantsIncludingSelf()
                .SelectMany(t => t.GetComponents<T>())
                .Where(c => c != null);
        }

        public static T GetComponentInAncestorsIncludingSelf<T>(this Component me) where T : Component
        {
            return GetComponentsInAncestorsIncludingSelf<T>(me)
                .FirstOrDefault();
        }

        public static IEnumerable<T> GetComponentsInAncestorsIncludingSelf<T>(this Component me) where T : Component
        {
            return me.transform.EnumerateAncestorsIncludingSelf()
                .SelectMany(t => t.GetComponents<T>())
                .Where(c => c != null);
        }

        public static Bounds ComputeVisibleBoundsFromRenderersInHierarchy(this Component me)
        {
            Bounds? result = null;
            var renderers = me.GetComponentsInDescendantsIncludingSelf<Renderer>();
            foreach (var renderer in renderers)
            {
                var bounds = renderer.bounds;
                if (result.HasValue)
                    result.Value.Encapsulate(bounds);
                else
                    result = bounds;
            }
            return result.Value;
        }

    }
}
