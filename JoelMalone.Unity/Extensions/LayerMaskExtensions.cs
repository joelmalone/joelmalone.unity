﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public static class LayerMaskExtensions
    {

        public static bool Includes(this LayerMask me, int layerIndex)
        {
            return (me.value & (1 << layerIndex)) != 0;
        }

    }

}