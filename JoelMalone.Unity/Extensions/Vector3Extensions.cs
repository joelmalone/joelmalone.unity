﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public static class Vector3Extensions
    {

        public static Bounds ToBounds(this IEnumerable<Vector3> me)
        {
            Bounds bounds = new Bounds();
            foreach (var p in me)
            {
                bounds.Encapsulate(p);
            }
            return bounds;
        }

        public static Vector3 FlattenX(this Vector3 me)
        {
            return new Vector3(0, me.y, me.z);
        }

        public static Vector3 FlattenY(this Vector3 me)
        {
            return new Vector3(me.x, 0, me.z);
        }

        public static Vector3 FlattenZ(this Vector3 me)
        {
            return new Vector3(me.x, me.y, 0);
        }

        public static Vector3 MatchX(this Vector3 me, Vector3 other)
        {
            return new Vector3(other.x, me.y, me.z);
        }

        public static Vector3 MatchY(this Vector3 me, Vector3 other)
        {
            return new Vector3(me.x, other.y, me.z);
        }

        public static Vector3 MatchZ(this Vector3 me, Vector3 other)
        {
            return new Vector3(me.x, me.y, other.z);
        }

        public static Vector3 WithX(this Vector3 me, float x)
        {
            return new Vector3(x, me.y, me.z);
        }

        public static Vector3 WithY(this Vector3 me, float y)
        {
            return new Vector3(me.x, y, me.z);
        }

        public static Vector3 WithZ(this Vector3 me, float z)
        {
            return new Vector3(me.x, me.y, z);
        }

        public static Rect ToCentredRect(this Vector3 me, float width, float height)
        {
            var rect = new Rect(me.x - width / 2, me.y - height / 2, width, height);
            return rect;
        }

        public static Vector3 ConstrainToBounds(this Vector3 me, Bounds bounds)
        {
            return new Vector3(
                Mathf.Clamp(me.x, bounds.min.x, bounds.max.x),
                Mathf.Clamp(me.y, bounds.min.y, bounds.max.y),
                Mathf.Clamp(me.z, bounds.min.z, bounds.max.z)
                );
        }

    }

}
