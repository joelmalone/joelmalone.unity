﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.Components;
using JoelMalone.Logging;

namespace JoelMalone.Unity
{

    public static class CameraExtensions
    {

        public static void FocusCameraOnGameObjectHierarchy(this Camera camera, GameObject gameObject)
        {
            FocusCameraOnGameObjectHierarchy(camera, gameObject, 1);
        }

        public static void FocusCameraOnGameObjectHierarchy(this Camera camera, GameObject gameObject, float distanceFactor)
        {
            Bounds b = gameObject.CalculateBoundsFromChildRenderers();
            FocusCameraOnBounds(camera, b, distanceFactor);
        }

        public static void FocusCameraOnBounds(this Camera camera, Bounds bounds)
        {
            FocusCameraOnBounds(camera, bounds, 1);
        }

        public static void FocusCameraOnBounds(this Camera camera, Bounds bounds, float distanceFactor)
        {
            Vector3 max = bounds.size;
            // Get the radius of a sphere circumscribing the bounds
            float radius = max.magnitude / 2f;

            FocusCameraOnBounds(camera, bounds.center, radius, distanceFactor);
        }

        public static void FocusCameraOnBounds(this Camera camera, Vector3 centre, float radius)
        {
            FocusCameraOnBounds(camera, centre, radius, 1);
        }

        public static void FocusCameraOnBounds(this Camera camera, Vector3 centre, float radius, float distanceFactor)
        {
            // Get the horizontal FOV, since it may be the limiting of the two FOVs to properly encapsulate the objects
            float horizontalFOV = camera.ComputeHorizontalFov();
            // Use the smaller FOV as it limits what would get cut off by the frustum   
            float fov = Mathf.Min(camera.fieldOfView, horizontalFOV);
            float dist = radius / (Mathf.Sin(fov * Mathf.Deg2Rad / 2f));

            //camera.D("Radius = " + radius + " dist = " + dist);

            //c.transform.SetLocalPositionZ(dist);
            //c.transform.localPosition = new Vector3(c.transform.localPosition.x, c.transform.localPosition.y, dist);

            if (camera.orthographic)
                camera.orthographicSize = radius;

            // Frame the object hierarchy
            camera.transform.LookAt(centre);

            camera.transform.position = centre -
                camera.transform.forward * dist * distanceFactor;

            //Debug.DrawLine(b.min, b.max, Color.white, 10);
        }

        public static Collider GetColliderAtScreenXy(this Camera me, Vector3 screenXy, int layerMask)
        {
            var ray = me.ScreenPointToRay(screenXy);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, float.MaxValue, layerMask))
            {
                //me.D("Hit: {0}.", hitInfo.collider.gameObject);
                return hitInfo.collider;
            }
            else
            {
                //me.D("No hit.");
                return null;
            }
        }

        public static T GetComponentAtScreenXy<T>(this Camera me, Vector3 screenXy) where T : Component
        {
            return GetComponentAtScreenXy<T>(me, screenXy, -1);
        }

        public static T GetComponentAtScreenXy<T>(this Camera me, Vector3 screenXy, int layerMask) where T : Component
        {
            var collider = GetColliderAtScreenXy(me, screenXy, layerMask);
            if (collider == null)
                return null;

            var result = collider.gameObject.GetComponent<T>();

            return result;
        }

        public static T GetComponentAtScreenXyAllowingRedirection<T>(this Camera me, Vector3 screenXy, int layerMask) where T : Component
        {
            return GetColliderAtScreenXy(me, screenXy, layerMask)
                .GetComponentAllowingRedirection<T>();
        }

        public static RaycastHit? GetHitAtScreenXy(this Camera me, Vector3 screenXy)
        {
            return GetHitAtScreenXy(me, screenXy, -1);
        }

        public static RaycastHit? GetHitAtScreenXy(this Camera me, Vector3 screenXy, int layerMask)
        {
            var ray = me.ScreenPointToRay(screenXy);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, float.MaxValue, layerMask))
            {
                return hitInfo;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Given the world position, returns a screen XY representing that position on the HUD, 
        /// bound to the edges of the screen when the position is off-screen.
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public static Vector3 WorldToHudBlip(this Camera camera, Vector3 worldPosition)
        {
            return WorldToHudBlip(camera, worldPosition, 15);
        }

        /// <summary>
        /// Given the world position, returns a screen XY representing that position on the HUD, 
        /// bound to the edges of the screen when the position is off-screen.
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="worldPosition"></param>
        /// <param name="edgeInsetPixels"></param>
        /// <returns></returns>
        public static Vector3 WorldToHudBlip(this Camera camera, Vector3 worldPosition, int edgeInsetPixels)
        {
            int screenWidth = Screen.width;
            int screenHeight = Screen.height;
            Vector3 screenCentre = new Vector3(screenWidth / 2, screenHeight / 2);

            // Define the factor to work in a square defined by sides -1..1
            var xFactor = 1 / (screenWidth * .5f - edgeInsetPixels);
            var yFactor = 1 / (screenHeight * .5f - edgeInsetPixels);

            var pos = camera.WorldToScreenPoint(worldPosition);
            pos -= screenCentre;
            if (pos.z < Mathf.Epsilon)
                // Flip when behind the camera, and also scale right out to ensure it 
                //  gets clamped, otherwise the blip will be in the position it would 
                //  be if it was on-screen
                pos *= -100000;

            // Scale from screen pixel coords to -1..1 as defined by the inset bounds
            pos.x *= xFactor;
            pos.y *= yFactor;
            pos.z = 0;

            // Flip the y, because the world's +y goes up the viewport, while the screen's +y goes down the viewport
            pos.y = -pos.y;

            // Clamp x and y to -1..1 (the bounds defined by edgeInsetPixels)
            if (pos.x < -1)
                pos /= -pos.x;
            else if (pos.x > 1)
                pos /= pos.x;
            if (pos.y < -1)
                pos /= -pos.y;
            else if (pos.y > 1)
                pos /= pos.y;

            // Set Z to 0..1 to identify whether the blip was on-screen, or was clamped
            pos.z = Mathf.Max(Mathf.Abs(pos.x), Mathf.Abs(pos.y));

            // Scale back to screen pixel coords
            pos.x /= xFactor;
            pos.y /= yFactor;

            return pos + screenCentre;
        }

        public static float ComputeHorizontalFov(this Camera me)
        {
            // http://forum.unity3d.com/threads/how-to-calculate-horizontal-field-of-view.16114/#post-110356
            var radAngle = me.fieldOfView * Mathf.Deg2Rad;
            var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * me.aspect);
            var hFOV = Mathf.Rad2Deg * radHFOV;
            return hFOV;
        }

        public static float ComputeFrustumHeightAtDistance(this Camera me, float distanceFromCamera)
        {
            // http://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
            var frustumHeight = 2.0f * distanceFromCamera * Mathf.Tan(me.fieldOfView * 0.5f * Mathf.Deg2Rad);
            return frustumHeight;
        }

        public static float ComputeFrustumWidthAtDistance(this Camera me, float distanceFromCamera)
        {
            // http://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
            var frustumHeight = 2.0f * distanceFromCamera * Mathf.Tan(me.fieldOfView * 0.5f * Mathf.Deg2Rad);
            return frustumHeight;
        }

        public static float ComputeFovFromDistanceAndHeight(this Camera me, float distanceFromCamera, float frustumHeight)
        {
            // http://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
            var fov = 2 * Mathf.Atan(frustumHeight * 0.5f / distanceFromCamera) * Mathf.Rad2Deg;
            return fov;
        }

        public static float ComputeFovFromDistanceAndWidth(this Camera me, float distanceFromCamera, float frustumWidth)
        {
            // http://docs.unity3d.com/Manual/FrustumSizeAtDistance.html
            var frustumHeight = frustumWidth / me.aspect;
            var fov = 2 * Mathf.Atan(frustumHeight * 0.5f / distanceFromCamera) * Mathf.Rad2Deg;
            return fov;
        }

        /// <summary>
        /// Given a camera's FOV, return the distance the camera must be to fit an imaginary 
        /// object of the specified height into view.
        /// </summary>
        /// <param name="me">The camera whose FOV will be used.</param>
        /// <param name="height">The height in world units, aligned to the camera's Y.</param>
        /// <returns>The distance the camera needs to be placed to fit the height into view.</returns>
        public static float ComputeDistanceToFitHeight(this Camera me, float height)
        {
            var distance = height / 2 / Mathf.Tan(me.fieldOfView * Mathf.Deg2Rad / 2);
            return distance;
        }

        public static Vector3 ComputeCoplanarScreenIntersection(this Camera me, Vector3 planeD, Vector2 screenXy)
        {
            Plane plane = new Plane(me.transform.forward, planeD);
            Ray ray = me.ScreenPointToRay(screenXy);
            float distance;
            if (plane.Raycast(ray, out distance))
            {
                return ray.GetPoint(distance);
            }
            else
            {
                // This "should never happen" under sane circumstances, unless the
                //  ray is parallel to the plane, which should be impossible for what
                //  it represents
                me.W("Unable to calculate coplanar inersection due to some kind of devilry!");
                return Vector3.zero;
            }
        }

        /// <summary>
        /// Computes the scale to apply to a transform's localScale, such that it will be the desired height
        /// when rendered to the screen.
        /// </summary>
        /// <param name="camera">The camera to use for calculations.</param>
        /// <param name="visibleBoundsCentre">The centre of the object's visible bounds in world units.</param>
        /// <param name="visibleBoundsHeight">The height of the visible bounds in world units.</param>
        /// <param name="desiredFrustumHeight">The desired height once rendered in normalised screen height.</param>
        /// <returns>The scale to apply to the object's localScale.</returns>
        public static float ComputeLocalScaleForFrustumHeightAtDistance(this Camera camera, Vector3 visibleBoundsCentre, float visibleBoundsHeight, float desiredFrustumHeight)
        {
            // Distance from the camera to the object
            var distanceToCamera = (visibleBoundsCentre - camera.transform.position).magnitude;
            // Frustum height in world units at this distance
            var frustumHeightAtDistance = camera.ComputeFrustumHeightAtDistance(distanceToCamera);
            // Normalised height of the object relative to frustum height
            var normalisedBoundsHeight = visibleBoundsHeight / frustumHeightAtDistance;
            // Finally, the scale to get from the original height to the desired height
            var scale = desiredFrustumHeight / normalisedBoundsHeight;

            return scale;
        }

    }

}