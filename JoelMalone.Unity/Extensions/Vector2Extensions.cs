﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public static class Vector2Extensions
    {

        public static Rect ToCentredRect(this Vector2 me, float width, float height)
        {
            var rect = new Rect(me.x - width / 2, me.y - height / 2, width, height);
            return rect;
        }

    }

}
