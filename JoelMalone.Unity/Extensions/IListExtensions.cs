﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity.Components;

namespace JoelMalone.Unity
{

    public static class IListExtensions
    {
        
        public static IList<T> SetAll<T>(this IList<T> me, T value)
        {
            for (int i = 0; i < me.Count; i++)
            {
                me[i] = value;
            }

            return me;
        }

    }

}