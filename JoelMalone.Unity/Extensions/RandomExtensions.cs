﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public static class RandomExtensions
    {

        public static Vector3 InsideUnitSphere(this System.Random random)
        {
            // The easiest way to create an uniformly-distributed set of points
            //  within a sphere is to just create points in a uniform cube, then 
            //  discard points outside the sphere

            Vector3 v;
            do
            {
                v = new Vector3(
                    (float)(random.NextDouble() * 2 - 1),
                    (float)(random.NextDouble() * 2 - 1),
                    (float)(random.NextDouble() * 2 - 1)
                    );
            } while (v.sqrMagnitude > 1);
            return v;
        }

    }

}