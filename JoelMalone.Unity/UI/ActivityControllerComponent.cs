﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using JoelMalone.Unity.Components;

namespace JoelMalone.Unity.UI
{

    /// <summary>
    /// Hosts the Activity system, relaying Unity events to it.
    /// </summary>
    public class ActivityControllerComponent : MonoBehaviour
    {

        private readonly ActivityController _controller = new ActivityController();

        public void Update()
        {
            _controller.NotifyUpdate();
        }

        public virtual void LateUpdate()
        {
            _controller.NotifyLateUpdate();
        }

        public ActivityController Controller { get { return _controller; } }

    }

}
