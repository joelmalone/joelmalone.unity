﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Logging;

namespace JoelMalone.Unity.UI
{

    public class Activity
    {

        public ActivityController Controller { get; internal set; }

        internal void NotifyOpened()
        {
            OnOpened();
            OnOpenedEvent();
        }
        internal void NotifyClosed()
        {
            OnClosed();
            OnClosedEvent();
        }
        internal void NotifySuspended()
        {
            OnSuspended();
            OnSuspendedEvent();
        }
        internal void NotifyResumed()
        {
            OnResumed();
            OnResumedEvent();
        }
        internal void NotifyActivated()
        {
            OnActivated();
            OnActivatedEvent();

            OnWireEvents();
        }
        internal void NotifyDeactivated()
        {
            // Unwire is called BEFORE OnDeactivated() in an "unwind" fashion
            OnUnwireEvents();

            OnDeactivated();
            OnDeactivatedEvent();
        }
        internal void NotifyUpdate()
        {
            OnUpdate();
        }
        internal void NotifyLateUpdate()
        {
            OnLateUpdate();
        }

        /// <summary>
        /// Called when the activity is opened.  Only called once per activity.
        /// </summary>
        protected virtual void OnOpened() { }
        /// <summary>
        /// Callled when the activity is closed.  Only called once per activity.
        /// </summary>
        protected virtual void OnClosed() { }
        /// <summary>
        /// Called when the activity is suspended so a child activity can take over.
        /// </summary>
        protected virtual void OnSuspended() { }
        /// <summary>
        /// Called when the activity resumes control after a child activity has closed.
        /// </summary>
        protected virtual void OnResumed() { }
        /// <summary>
        /// Called when the activity takes control, for both Open and Resume situtations.
        /// </summary>
        protected virtual void OnActivated() { }
        /// <summary>
        /// Called whenever the activity loses control, for both Close and Suspend situations.
        /// </summary>
        protected virtual void OnDeactivated() { }

        /// <summary>
        /// Unity's Update event relayed from ActivityControllerComponent to the current activity.
        /// </summary>
        protected virtual void OnUpdate() { }
        /// <summary>
        /// Unity's LateUpdate event relayed from ActivityControllerComponent to the current activity.
        /// </summary>
        protected virtual void OnLateUpdate() { }

        /// <summary>
        /// Called immediately after OnActivated().
        /// </summary>
        protected virtual void OnWireEvents()
        {
        }
        /// <summary>
        /// Called immediately before OnDeactivated().
        /// </summary>
        protected virtual void OnUnwireEvents()
        {
        }

        public event Action OnOpenedEvent = delegate { };
        public event Action OnClosedEvent = delegate { };
        public event Action OnActivatedEvent = delegate { };
        public event Action OnDeactivatedEvent = delegate { };
        public event Action OnSuspendedEvent = delegate { };
        public event Action OnResumedEvent = delegate { };

        public void Close()
        {
            if (Controller.Current == this)
                Controller.CloseTop();
            else
                this.W("Attempt to Close() non-top ativity has been ignored.");
        }

        public override string ToString()
        {
            return GetType().Name;
        }

    }

    //public static class ActivityExtensions
    //{

    //    public static void Open<TContext>(this Activity<TContext> me) where TContext : IActivityContext
    //    {
    //        me.Controller.Stack.Open(me);
    //    }

    //    public static void Close<TContext>(this Activity<TContext> me) where TContext : IActivityContext
    //    {
    //        if (!ReferenceEquals(me, me.Controller.Stack.Current))
    //            me.W("Tried to close activity, but it is not the current activity!  Stack: {0}", me.Controller.Stack.ToString());
    //        else
    //            me.Controller.Stack.CloseTop();
    //    }

    //    public static void OpenClearAll<TContext>(this Activity<TContext> me) where TContext : IActivityContext
    //    {
    //        me.Controller.Stack.CloseAll();
    //        me.Controller.Stack.Open(me);
    //    }


    //}

}
