﻿using JoelMalone.Unity.Components;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using JoelMalone.Unity.UI;

namespace JoelMalone.Unity.UI.Activities
{

    public class WaitActivity : Activity
    {

        private readonly Func<float> _timeGetter;
        private readonly float _completeTime;
        private readonly Action _doneCallback;

        public WaitActivity(Func<float> timeGetter, float seconds, Action doneCallback)
        {
            _timeGetter = timeGetter;
            _completeTime = _timeGetter() + seconds;
            _doneCallback = doneCallback;
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();

            var time = _timeGetter();
            if (_timeGetter() >= _completeTime)
                _doneCallback();
        }

    }

}
