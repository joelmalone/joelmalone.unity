﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Logging;

namespace JoelMalone.Unity.UI
{

    public class ActivityController
    {

        private readonly Stack<Activity> _activities = new Stack<Activity>();
        private readonly Activity _nullActivity = new Activity();

        public Activity Current
        {
            get
            {
                if (_activities.Count > 0)
                    return _activities.Peek();
                else
                    return _nullActivity;
            }
        }

        public void Open(Activity activity)
        {
            if (_activities.Count > 0)
            {
                var current = _activities.Peek();
                if (activity == current)
                {
                    this.W("Activity {0} is already the top activity.");
                    return;
                }

                this.D("Deactivating and suspending activity {0}.", current);

                current.NotifyDeactivated();
                current.NotifySuspended();
            }

            _activities.Push(activity);

            this.D("Opening and activating activity {0}.", activity);

            activity.Controller = this;
            activity.NotifyOpened();
            activity.NotifyActivated();

            //this.D("Activity stack is now {0}.", this);
        }

        public void CloseTop()
        {
            if (_activities.Count == 0)
            {
                this.W("CloseTop(), but there are no activities in the stack.");
                return;
            }

            var current = _activities.Pop();
            var resumed = _activities.Peek();

            this.D("Deactivating and closing activity {0}.", current);

            current.NotifyDeactivated();
            current.NotifyClosed();

            if (_activities.Count == 0)
                return;

            // Only resume+activate the resumed activity if it still
            //  is the top activity; the closed activity's event may
            //  have launched a new activity in the previous few lines
            //  of code :/
            if (resumed == _activities.Peek())
            {
                this.D("Resuming and activating activity {0}.", resumed);

                resumed.NotifyResumed();
                resumed.NotifyActivated();
            }

            //this.D("Resumed activity {0}.", resumed);
        }

        public void CloseAll()
        {
            var count = 0;

            if (_activities.Count > 0)
            {
                var current = _activities.Pop();
                current.NotifyDeactivated();
                current.NotifyClosed();
                count++;

                while (_activities.Count > 0)
                {
                    current = _activities.Pop();
                    current.NotifyResumed();
                    current.NotifyClosed();
                    count++;
                }
            }

            this.D("PopAllActivities().  {0} activities popped.", count);
        }

        public override string ToString()
        {
            var segments = from a in _activities.Reverse()
                           select a.GetType().Name;
            var result = String.Join(" > ", segments.ToArray());
            return result;
        }

        public void NotifyUpdate()
        {
            Current.NotifyUpdate();
        }

        public void NotifyLateUpdate()
        {
            Current.NotifyLateUpdate();
        }
    }
}
