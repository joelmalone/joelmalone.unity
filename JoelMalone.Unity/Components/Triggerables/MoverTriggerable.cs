﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using System.Collections;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// When triggered, lerps this gameObject to the transform 
    /// defined by TriggeredState.  When untriggered, lerps this 
    /// gameObject to the transform defined by UntriggeredState.
    /// Either property can be null, in which case the gameObject's
    /// own transform at startup is used.
    /// </summary>
    public class MoverTriggerable : TriggerableComponent
    {

        [Required]
        public Transform TriggeredState;
        public Transform UntriggeredState;

        /// <summary>
        /// How long the move takes, for both triggering and untriggering.
        /// </summary>
        public float MoveDuration = 5f;
        /// <summary>
        /// When untriggered, how long to wait before moving to UntriggeredState.  Useful 
        /// for doors that should delay a bit before closing.
        /// </summary>
        public float UntriggerDelay = 0;

        private Vector3 _triggeredPosition;
        private Vector3 _untriggeredPosition;

        public override void Awake()
        {
            base.Awake();

            _triggeredPosition = TriggeredState ? TriggeredState.position : transform.position;
            _untriggeredPosition = UntriggeredState ? UntriggeredState.position : transform.position;

            if(_triggeredPosition == _untriggeredPosition)
                this.W("Triggered and Untriggered states are the same ({0}).", _triggeredPosition);
        }

        private IEnumerator DelayedCloseC()
        {
            if (UntriggerDelay > 0)
                yield return new WaitForSeconds(UntriggerDelay);

            var moveTime = Mathf.Max(0.1f, MoveDuration);
            yield return StartCoroutine(this.transform.MoveTo(_untriggeredPosition, moveTime));
        }

        protected internal override void OnTrigger()
        {
            if (!TriggeredState)
                return;

            var moveTime = Mathf.Max(0.1f, MoveDuration);

            StopAllCoroutines();
            StartCoroutine(this.transform.MoveTo(_triggeredPosition, moveTime));
        }

        protected internal override void OnUntrigger()
        {
            StopAllCoroutines();
            StartCoroutine(DelayedCloseC());
        }
    }

}
