﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// When triggered, copies the specified Transform attributes 
    /// from Source to this component's gameObject.
    /// </summary>
    public class CopyTransformTriggerable : TriggerableComponent
    {

        [Required]
        public Transform Source;

        public bool CopyPosition = true;
        public bool CopyRotation = true;
        public bool CopyLocalScale = false;

        protected internal override void OnTrigger()
        {
            if (CopyPosition)
                transform.position = Source.position;
            if(CopyRotation)
                transform.rotation = Source.rotation;
            if(CopyLocalScale)
                transform.localScale = Source.localScale;
        }

        protected internal override void OnUntrigger()
        {
        }
    }

}
