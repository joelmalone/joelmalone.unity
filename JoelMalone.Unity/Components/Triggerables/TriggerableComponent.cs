﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using JoelMalone.Unity.Attributes;
using System.Linq;
using System;
using System.Collections.Generic;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// Base class for a triggerable thing.  Triggers receive Triggered() and
    /// Untriggered() events, depending on the triggering object.
    /// For example, a pressure plate on the floor might call Trigger() when
    /// the player steps on it, and Untrigger() when the player steps off.
    /// A timed button may call Trigger() when pressed, and then Untrigger()
    /// a few seconds later.  Some triggers may never call Untrigger().
    /// Triggers and triggerables are located by a string; multiple triggers
    /// and triggerables can share a name - they do not have to be unique.
    /// </summary>
    public abstract class TriggerableComponent : MonoBehaviour
    {

        public string TriggerName;

        internal static readonly List<TriggerableComponent> All = new List<TriggerableComponent>();

        public virtual void Awake()
        {
            All.Add(this);

            this.ExecuteBootstrapAttributes();

            this.D("Awake().");
        }

        public virtual void OnDestroy()
        {
            All.Remove(this);
        }

        internal protected abstract void OnTrigger();
        internal protected abstract void OnUntrigger();

    }

    public static class TriggerableExtensions
    {

        /// <summary>
        /// Broadcast a trigger event for the specified trigger.
        /// </summary>
        /// <param name="me">The component broadcasting the trigger.</param>
        /// <param name="triggerName">The name of the trigger to broadcast.</param>
        public static void BroadcastTrigger(this Component me, string triggerName)
        {
            if (triggerName == null)
                throw new ArgumentNullException();

            me.D("BroadcastTrigger(\"{0}\").", triggerName);
            me.D("Triggerables: {0}.", TriggerableComponent.All.Count);

            var targets = from t in TriggerableComponent.All
                          where triggerName.Equals(t.TriggerName)
                          select t;
            foreach (var t in targets)
            {
                t.D("OnTrigger().");
                t.OnTrigger();
            }
        }

        /// <summary>
        /// Broadcast an untrigger event for the specified trigger.
        /// </summary>
        /// <param name="me">The component broadcasting the trigger.</param>
        /// <param name="triggerName">The name of the trigger to broadcast.</param>
        public static void BroadcastUntrigger(this Component me, string triggerName)
        {
            if (triggerName == null)
                throw new ArgumentNullException();

            me.D("BroadcastUntrigger(\"{0}\").", triggerName);

            var targets = from t in TriggerableComponent.All
                          where triggerName.Equals(t.TriggerName)
                          select t;
            foreach (var t in targets)
            {
                t.D("OnUntrigger().");
                t.OnUntrigger();
            }
        }

    }

}
