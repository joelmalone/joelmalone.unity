﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// When triggered, immediately sets the position and rotation of the
    /// targeted CameraAnimatorComponent to the values in TargetPosition,
    /// and clears the camera's FocusTarget property.  Does nothing when
    /// untriggered.
    /// </summary>
    public class UpdateCameraAnimatorTriggerable : TriggerableComponent
    {

        [Required]
        public CameraAnimatorComponent CameraAnimator;

        [Required]
        public Transform TargetPosition;

        protected internal override void OnTrigger()
        {
            CameraAnimator.transform.position = TargetPosition.position;
            CameraAnimator.transform.rotation = TargetPosition.rotation;
            CameraAnimator.FocusTarget = null;
        }

        protected internal override void OnUntrigger()
        {
        }
    }

}
