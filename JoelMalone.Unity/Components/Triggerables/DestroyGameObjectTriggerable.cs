﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// When triggered, calls GameObject.Destroy(me.gameObject).
    /// </summary>
    public class DestroyGameObjectTriggerable : TriggerableComponent
    {

        protected internal override void OnTrigger()
        {
            this.DestroyGameObject();
        }

        protected internal override void OnUntrigger()
        {
        }

    }

}
