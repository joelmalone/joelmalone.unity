﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components.Triggerables
{

    /// <summary>
    /// When triggered and untriggered, toggles the activeSelf state
    /// of all child gameobjects.
    /// </summary>
    public class ToggleChildrenActiveTriggerable : TriggerableComponent
    {

        protected internal override void OnTrigger()
        {
            foreach (var child in this.GetChildGameObjects())
            {
                child.SetActive(!child.activeSelf);
            }
        }

        protected internal override void OnUntrigger()
        {
            foreach(var child in this.GetChildGameObjects())
            {
                child.SetActive(!child.activeSelf);
            }
        }

    }

}
