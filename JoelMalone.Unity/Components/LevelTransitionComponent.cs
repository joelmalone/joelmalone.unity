﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using JoelMalone.Unity.Attributes;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Transitions between levels with a fade in/out effect.  The component
    /// fades the scene in upon Start() and out when TransitionToLevel() is
    /// called.  Place one of these into all of your levels.
    /// </summary>
    [RequireComponent(typeof(GUITexture))]
    public class LevelTransitionComponent : MonoBehaviour
    {

        /// <summary>
        /// For some reason the texture is opaque at .5f alpha, so fade to that.
        /// </summary>
        private readonly static Color _blendColour = new Color(1, 1, 1, .5f);
        private readonly static Color _clearColour = _blendColour.WithAlpha(0);

        public GUITexture GUITexture;
        public float BlendInDelay = .15f;
        public float BlendInOutTime = 1;

        private bool _isTransitioning;

        public void Awake()
        {
            GUITexture.enabled = true;
            ApplyColour(_blendColour);
        }

        public IEnumerator Start()
        {
            if (BlendInDelay > Mathf.Epsilon)
                yield return new WaitForSeconds(BlendInDelay);
            yield return StartCoroutine(BlendToClearC());
        }

        /// <summary>
        /// Helpers method to transition to the next level, or back to level 
        /// 0 if no more levels.  Call this rather than hunting for the 
        /// instance in your scene.
        /// </summary>
        public static void TransitionToNextLevelWithWrapping()
        {
            var next = Application.loadedLevel + 1;
            next %= Application.levelCount;
            TransitionToLevel(next);
        }

        /// <summary>
        /// Helper method to transitions to the level specified.  Call this
        /// rather than hunting for the instance in your scene.
        /// </summary>
        /// <param name="levelIndex"></param>
        public static void TransitionToLevel(int levelIndex)
        {
            var instance = FindObjectOfType<LevelTransitionComponent>();
            if (!instance)
            {
                Log.Write(typeof(LevelTransitionComponent), LogLevel.Warning, "Did not find an instance of LevelTransitionComponent, so just changing level the ol' fashioned way.");
                Application.LoadLevel(levelIndex);
            }
            else
            {
                instance.StopAllCoroutines();
                instance.StartCoroutine(instance.TransitionToLevelC(levelIndex));
            }
        }

        private IEnumerator BlendToClearC()
        {
            GUITexture.enabled = true;
            yield return StartCoroutine(_blendColour.BlendToC(_clearColour, BlendInOutTime, ApplyColour));
            GUITexture.enabled = false;
        }

        private IEnumerator BlendToColourC()
        {
            GUITexture.enabled = true;
            yield return StartCoroutine(_clearColour.BlendToC(_blendColour, BlendInOutTime, ApplyColour));
        }

        private void ApplyColour(Color colour)
        {
            GUITexture.color = colour;
        }

        private IEnumerator TransitionToLevelC(int levelIndex)
        {
            // Prevent multiple transitions, e.g. when someone double-taps
            //  a level button - it's easier to catch here than everywhere else
            if (_isTransitioning)
            {
                this.W("Ignoring transition request to level {0} as a transition is already in progress.", levelIndex);
                yield break;
            }
            _isTransitioning = true;

            yield return StartCoroutine(BlendToColourC());

            this.D("Loading level {0}.", levelIndex);
            try
            {
                // A TypeLoadException will occur here for non-handheld builds, which we
                //  catch and ignore in this case.  We wrap the sensitive code in a delegate,
                //  as TypeLoadExceptions are fired at the beginning of the method, where
                //  we can't easily catch them (at least, not inline).
                Action del = () =>Handheld.StartActivityIndicator();
                del();
            } catch(Exception e)
            {
                this.E(e, "Exception while calling Handheld.StartActivityIndicator().");
            }

            Application.LoadLevel(levelIndex);

            // Normally we'd just end here, as the new level's 
            //  LevelTransitionComponent will fade the scene in, 
            //  but we will stick around a bit longer to get 
            //  some timing info

            DontDestroyOnLoad(this);

            // Stick around so we can time the load
            var start = Time.realtimeSinceStartup;
            while (Application.isLoadingLevel)
                yield return null;
            var elpased = Time.realtimeSinceStartup - start;
            
            this.D("Loaded level {0} \"{1}\" in {2:0.0}s.", Application.loadedLevel, Application.loadedLevelName, elpased);

            Destroy(gameObject);
        }

    }

}
