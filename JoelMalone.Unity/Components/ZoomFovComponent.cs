﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Attach this to a camera to scale the camera's fieldOfView based on the
    /// camera's distance to the specified Target.
    /// For example, at a distance equal to BaseDistance, fieldOfView will be 
    /// set to BaseFov; at half the distance, fieldOfView will be set to half 
    /// of BaseFov; at double the distance, fieldOfView will be set to 2 * BaseFov.
    /// This allows some rudumentary "zooming in" when tracking an object from 
    /// a fixed position.  You can use RotateToFaceGameObjectComponent to handle 
    /// rotation updates.
    /// </summary>
    public class ZoomFovComponent : MonoBehaviour
    {

        public Camera Camera;
        public Transform Target;

        public float BaseDistance = 10;
        public float BaseFov = 60;

        public void LateUpdate()
        {
            if (Target)
            {
                var dist = Vector3.Distance(Target.position, transform.position);
                var distFactor = BaseDistance / dist;
                var fov = distFactor * BaseFov;
                Camera.fieldOfView = fov;
            }
        }

    }

}