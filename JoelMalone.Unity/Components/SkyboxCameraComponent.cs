﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using JoelMalone.Unity.Attributes;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Attach this to a skybox camera to mimic movement of the main scene camera,
    /// providing a parallax effect, making the skybox seems less "fixed".
    /// Requires a SkyboxAnchorComponent existing in the scene.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class SkyboxCameraComponent : MonoBehaviour
    {

        [Required]
        public Camera MainCamera;

        [AutoFromScene]
        protected SkyboxAnchorComponent _anchor;

        public float MoveFactor = .001f;
        public float MaxMoveRange = .3f;

        private Vector3 _startPosition;

        public void Start()
        {
            this.ExecuteBootstrapAttributes();

            _startPosition = transform.position;
        }

        public void LateUpdate()
        {
            if (!MainCamera || !_anchor)
                return;

            transform.rotation = MainCamera.transform.rotation;

            var delta = (MainCamera.transform.position - _anchor.transform.position) * MoveFactor;
            var mag = delta.magnitude;
            if (mag > MaxMoveRange)
                delta = delta / mag * MaxMoveRange;
            transform.position = _startPosition + delta;
        }

    }

}
