﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Handles graceful camera movement through a range of helper methods.
    /// Also introduces the concept of a focal point or target, to allow
    /// advanced camera manipulation, e.g. orbiting, pixel-perfect panning,
    /// etc.
    /// Methods are fluent, so you can chain them together easily.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class CameraAnimatorComponent : MonoBehaviour
    {

        [Required]
        public Camera SlaveCamera;
        public Camera Camera;

        public Vector3 FocusPoint;
        public Transform FocusTarget;

        public float MoveFactorPerSecond = 5;
        public float TurnFactorPerSecond = 5;

        public void Awake()
        {
            // The camera animator should not be attached to a camera used 
            //  for rendering; rather, it should be attached to an empty 
            //  gameobject, which the rendering camera is interpolated to
            if (Camera && Camera.transform == transform)
            {
                this.W("CameraAnimatorComponent should not be placed on an active camera.  Instead, place it on an empty game object and then specify the SlaveCamera to be manipulated.");
            }
        }

        public void LateUpdate()
        {
            if (FocusTarget)
            {
                FocusPoint = FocusTarget.position;
                transform.LookAt(FocusPoint);
            }

            if (SlaveCamera)
            {
                var moveFactor = Mathf.Max(0.01f, MoveFactorPerSecond);
                SlaveCamera.transform.position = Vector3.Lerp(SlaveCamera.transform.position, transform.position, Mathf.Clamp01(moveFactor * Time.deltaTime));

                var turnFactor = Mathf.Max(0.01f, TurnFactorPerSecond);
                SlaveCamera.transform.rotation = Quaternion.Slerp(SlaveCamera.transform.rotation, transform.rotation, Mathf.Clamp01(turnFactor * Time.deltaTime));
            }
        }

        public CameraAnimatorComponent SetFocus(Vector3 point)
        {
            FocusPoint = point;
            FocusTarget = null;

            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent SetFocus(Transform target)
        {
            FocusPoint = target.position;
            FocusTarget = target;

            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent SetFocus(Component target)
        {
            FocusPoint = target.transform.position;
            FocusTarget = target.transform;

            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent AdjustFocus(Vector3 adjustment)
        {
            FocusPoint += adjustment;
            FocusTarget = null;

            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent PanTo(Vector3 point)
        {
            var diff = point - transform.position;

            FocusPoint += diff;
            FocusTarget = null;

            transform.position = point;

            return this;
        }

        public CameraAnimatorComponent PanToFocus(Vector3 focus)
        {
            var diff = focus - FocusPoint;

            FocusPoint = focus;
            FocusTarget = null;

            transform.position += diff;

            return this;
        }

        public CameraAnimatorComponent PanInScreenSpace(Vector3 screenXY)
        {
            var adjustment = screenXY.x * transform.right
                + screenXY.y * transform.up
                + screenXY.z * transform.forward;

            FocusPoint += adjustment;
            FocusTarget = null;

            transform.position += adjustment;
            
            return this;
        }

        public CameraAnimatorComponent SetRange(float range)
        {
            var toFocus = (FocusPoint - transform.position);
            transform.position = FocusPoint - toFocus.normalized * range;
            return this;
        }

        public CameraAnimatorComponent SetRangeWithYOffset(float horizontalRange, float yOffset)
        {
            var toFocus = (FocusPoint - transform.position).FlattenY();
            transform.position = FocusPoint 
                - toFocus.normalized * horizontalRange 
                + Vector3.up * yOffset;

            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent AdjustRangeByFactor(float adjustmentFactor)
        {
            var toFocus = (FocusPoint - transform.position) * adjustmentFactor;
            transform.position = FocusPoint - toFocus;
            return this;
        }

        public CameraAnimatorComponent Orbit(float rightDegrees, float upDegrees)
        {
            //this.D("Orbit() rightDegrees: {0} upDegrees: {1}.", rightDegrees, upDegrees);

            var range = (FocusPoint - transform.position).magnitude;
            transform.Rotate(upDegrees, -rightDegrees, 0);
            transform.position = FocusPoint - transform.forward * range;
            transform.LookAt(FocusPoint);

            return this;
        }

        public CameraAnimatorComponent MoveTo(Vector3 position)
        {
            transform.position = position;

            return this;
        }

        /// <summary>
        /// Moves the camera near movePosition, looking towards focusPosition, keeping
        /// both positions on-screen.
        /// </summary>
        /// <param name="movePosition"></param>
        /// <param name="focusPosition"></param>
        /// <returns></returns>
        public CameraAnimatorComponent FramePositions(Vector3 movePosition, Vector3 focusPosition, float offset)
        {
            // TODO: needs work!

            transform.position = movePosition;
            transform.LookAt(focusPosition);
            transform.position += transform.right * offset + transform.up * offset;
            transform.LookAt(focusPosition);

            return this;
        }

        public CameraAnimatorComponent FocusCameraOnBounds(Bounds bounds)
        {
            return FocusCameraOnBounds(bounds, 1);
        }

        public CameraAnimatorComponent FocusCameraOnBounds(Bounds bounds, float distanceFactor)
        {
            Vector3 max = bounds.size;
            // Get the radius of a sphere circumscribing the bounds
            float radius = max.magnitude / 2f;

            return FocusCameraOnBounds(bounds.center, radius, distanceFactor);
        }

        public CameraAnimatorComponent FocusCameraOnBounds(Vector3 centre, float radius)
        {
            return FocusCameraOnBounds(centre, radius, 1);
        }

        public CameraAnimatorComponent FocusCameraOnBounds(Vector3 centre, float radius, float distanceFactor)
        {
            // Note use of "camera" - we're setting the "disabled" camera, not the 
            //  actual camera, "Camera"
            Camera.FocusCameraOnBounds(centre, radius, distanceFactor);
            FocusPoint = centre;
            FocusTarget = null;

            return this;
        }

        /// <summary>
        /// Sets the camera rotation as specified, maintaining focus and range.
        /// </summary>
        /// <param name="rotation"></param>
        /// <returns></returns>
        public CameraAnimatorComponent SetRotation(Quaternion rotation)
        {
            var range = (FocusPoint - transform.position).magnitude;
            transform.rotation = rotation;
            transform.position = FocusPoint - transform.forward * range;

            return this;
        }

        public void CopyFrom(Camera source)
        {
            Camera.CopyFrom(source);
        }
    }

}
