﻿using UnityEngine;
using System.Collections;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Attach to a camera to allow zooming via touch and mouse.
    /// </summary>
    public class TouchZoomCameraComponent : MonoBehaviour
    {

        public float scaleFactor = 10;
        public float maxZoom = 20;

        public Camera Camera;

        public void LateUpdate()
        {
            if (Input.touchCount == 2)
            {
                var touch1 = Input.GetTouch(0);
                var touch2 = Input.GetTouch(1);

                var localScale = transform.localScale;

                if (touch1.position.y < touch2.position.y)
                {
                    localScale.x -= (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;
                    localScale.y -= (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;
                    localScale.z -= (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;

                }
                if (touch1.position.y > touch2.position.y)
                {
                    localScale.x += (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;
                    localScale.y += (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;
                    localScale.z += (touch1.deltaPosition.x - touch2.deltaPosition.x) / scaleFactor;
                }

                if (localScale.x >= maxZoom)
                    localScale.x = maxZoom;
                if (localScale.y >= maxZoom)
                    localScale.y = maxZoom;
                if (localScale.z >= maxZoom)
                    localScale.z = maxZoom;
                if (localScale.x <= 0.4)
                    localScale.x = 0.4f;
                if (localScale.y <= 0.4)
                    localScale.y = 0.4f;
                if (localScale.z <= 0.4)
                    localScale.z = 0.4f;

                transform.localScale = localScale;
                Camera.fieldOfView = localScale.z * 60;
            }

            if (Input.GetAxis("Mouse ScrollWheel") < 0)
            {
                transform.localScale *= 1.1f;
            }
        }
    }

}