﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using System.Collections;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Finds and follows the first object with tag "Player", by updating
    /// it's own position in Update().
    /// </summary>
    public class PlayerLojackComponent : MonoBehaviour
    {

        public GameObject Player;

        private void Update()
        {
            if (!Player)
            {
                Player = GameObject.FindGameObjectWithTag("Player");
            }
            if (Player)
            {
                transform.position = Player.transform.position;
            }
        }

    }

}
