﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Disables an attached Renderer at Awake().
    /// </summary>
    public class HideAtAwakeComponent : MonoBehaviour
    {

        public void Awake()
        {
            var renderer = this.GetComponent<Renderer>();
            if (renderer)
                renderer.enabled = false;
        }

    }

}
