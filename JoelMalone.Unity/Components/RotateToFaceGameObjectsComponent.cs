﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Uses LateUpdate() to update the gameobject's rotation to face 
    /// the average position of the target objects.
    /// </summary>
    public class RotateToFaceGameObjectsComponent : MonoBehaviour
    {

        public Transform[] Targets = new Transform[0];

        public void Start()
        {
            if (Targets.Length == 0)
            {
                this.W("No targets set.");
            }
        }

        public void LateUpdate()
        {
            if (Targets.Length == 0)
                return;

            Vector3 centre = Vector3.zero;
            foreach (var t in Targets)
                centre += t.position;
            centre /= Targets.Length;

            transform.LookAt(centre);
        }

    }

}