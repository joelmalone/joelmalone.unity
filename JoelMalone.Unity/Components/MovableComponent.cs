﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.Attributes;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Handles lerping between transforms.  This class is superceded - use 
    /// the UnityPattern.Auto extensions methods, or MoverTriggerable instead.
    /// </summary>
    public class MovableComponent : MonoBehaviour
    {

        public AnimationCurve Curve = AnimationCurve.EaseInOut(0, 0, 1, 1);
        public float MoveTime = 1.5f;

        Vector3 _fromPosition, _toPosition;
        Quaternion _fromRotation, _toRotation;
        Vector3 _fromScale, _toScale;
        float _startTime, _endTime;
        bool _isComplete;

        public void Awake()
        {
            this.ExecuteBootstrapAttributes();
            enabled = false;
        }

        public void Move(float moveTime, Vector3 fromPosition, Quaternion fromRotation, Vector3 fromScale, Vector3 toPosition, Quaternion toRotation, Vector3 toScale)
        {
            if (float.IsNaN(fromPosition.x))
                throw new ArgumentException();
            if (float.IsNaN(toPosition.x))
                throw new ArgumentException();

            if (moveTime > 0)
                MoveTime = moveTime;

            _fromPosition = fromPosition;
            _fromRotation = fromRotation;
            _fromScale = fromScale;
            _toPosition = toPosition;
            _toRotation = toRotation;
            _toScale = toScale;

            _startTime = Time.time;
            _endTime = Time.time + MoveTime;

            _isComplete = false;

            enabled = true;
        }

        public void Move(Vector3 toPosition, Quaternion toRotation, Vector3 toScale, float moveTime = -1)
        {
            Move(moveTime, transform.position, transform.rotation, transform.localScale, toPosition, toRotation, toScale);
        }

        public void Move(Component target, float moveTime = -1)
        {
            Move(target.transform.position, target.transform.rotation, target.transform.localScale, moveTime);
        }

        public void Update()
        {
            float elapsed = Time.time - _startTime;
            float duration = _endTime - _startTime;
            float t = Mathf.Clamp01(elapsed / duration);

            if (Mathf.Approximately(1, t))
            {
                transform.position = _toPosition;
                transform.rotation = _toRotation;
                transform.localScale = _toScale;

                _isComplete = true;
                enabled = false;
            }
            else
            {
                float y = Curve.Evaluate(t);

                //this.D(
                //    "Move t = {0:0.000}, y = {1:0.000}, from = {2}, {3}, {4}, to = {5}, {6}, {7}.",
                //    t,
                //    y,
                //    _fromPosition,
                //    _fromRotation,
                //    _fromScale,
                //    _toPosition,
                //    _toRotation,
                //    _toScale
                //    );

                Vector3 pos = Vector3.Lerp(_fromPosition, _toPosition, y);
                Quaternion rot = Quaternion.Lerp(_fromRotation, _toRotation, y);
                Vector3 s = Vector3.Lerp(_fromScale, _toScale, y);

                transform.position = pos;
                transform.rotation = rot;
                transform.localScale = s;
            }
        }

        public bool IsComplete { get { return _isComplete; } }

    }

}
