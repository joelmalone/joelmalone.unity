﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;
using System.Collections;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Cycles through a series of camera position, each defined by a Camera 
    /// in the scene, pausing at each one before lerping to the next.
    /// Can abort the cutscene by pressing esacpe.
    /// </summary>
    public class PointOfInterestCutsceneComponent : MonoBehaviour
    {

        public Camera[] PointsOfInterest;
        public float MoveDuration = 2;
        public float PauseDuration = 1;
        public EaseType EaseType = EaseType.QuadInOut;
        public Signal TriggerOnComplete;
        public Camera Camera;

        public IEnumerator Start()
        {
            if (PointsOfInterest == null || PointsOfInterest.Length == 0)
                yield break;

            foreach (var poi in PointsOfInterest)
            {
                var moveDuration = Mathf.Max(0.1f, MoveDuration);
                yield return StartCoroutine(Camera.transform.MoveAndRotateToC(poi.transform, moveDuration, Ease.FromType(EaseType)));

                if (PauseDuration > 0)
                    yield return StartCoroutine(Auto.Wait(PauseDuration));
            }

            OnComplete();
        }

        public void Update()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                this.D("Aborting cutscene.");

                StopAllCoroutines();

                var lastPoi = PointsOfInterest.LastOrDefault();
                if(lastPoi)
                {
                    Camera.transform.position = lastPoi.transform.position;
                    Camera.transform.rotation = lastPoi.transform.rotation;
                }

                OnComplete();
            }
        }

        private void OnComplete()
        {
            if (TriggerOnComplete != null)
            {
                TriggerOnComplete.Entered();
            }

            enabled = false;
        }

    }

}
