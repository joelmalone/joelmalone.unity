﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Handles lerping between positions.  This class is superceded - use 
    /// the UnityPattern.Auto extensions methods, or MoverTriggerable instead.
    /// </summary>
    public class MoveToTargetComponent : MonoBehaviour
    {
        public Transform Target;
        public float MoveFactor = 0.5f;

        public bool OnFixedUpdate;

        public void LateUpdate()
        {
            if (!OnFixedUpdate)
                Do();
        }

        public void FixedUpdate()
        {
            if (OnFixedUpdate)
                Do();
        }

        private void Do()
        {
            if (Target != null)
            {
                var target = Target.position;
                var current = transform.position;
                var moveFactor = Mathf.Max(0.01f, MoveFactor);

                transform.position = Vector3.Slerp(current, target, moveFactor * Time.deltaTime);
            }
        }

    }

}