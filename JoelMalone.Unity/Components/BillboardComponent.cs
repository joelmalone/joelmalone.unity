﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Attaching this to a gameobject will set the gameobject's rotation to 
    /// match the camera's rotation every LateUpdate(), optionally scaling
    /// the gameobject to achieve an on-screen height.
    /// </summary>
    public class BillboardComponent : MonoBehaviour
    {

        public float DesiredScreenHeightInches = 1;
        
        private Bounds _initialRendererBounds;

        public void Start()
        {
            _initialRendererBounds = this.ComputeVisibleBoundsFromRenderersInHierarchy();
        }

        public void LateUpdate()
        {
            transform.rotation = Camera.main.transform.rotation;

            if(DesiredScreenHeightInches > 0)
            {
                var normalisedDesiredHeight = (DesiredScreenHeightInches * ScreenHelpers.DpiSafe) / Screen.height;
                var scale = Camera.main.ComputeLocalScaleForFrustumHeightAtDistance(
                    transform.position, 
                    _initialRendererBounds.size.y, 
                    normalisedDesiredHeight
                    );

                transform.localScale = scale * Vector3.one;
            }
        }

    }

}
