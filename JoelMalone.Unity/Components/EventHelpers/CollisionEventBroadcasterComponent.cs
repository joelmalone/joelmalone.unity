﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components.EventHelpers
{

    public class CollisionEventBroadcasterComponent : MonoBehaviour
    {

        public delegate void CollisionEventDelegate(Component receiver, Collision collision);

        public event CollisionEventDelegate OnCollisionEnterEvent;
        public event CollisionEventDelegate OnCollisionStayEvent;
        public event CollisionEventDelegate OnCollisionExitEvent;

        public void OnCollisionEnter(Collision collision)
        {
            if (OnCollisionEnterEvent != null)
                OnCollisionEnterEvent(this, collision);
        }

        public void OnCollisionStay(Collision collision)
        {
            if (OnCollisionStayEvent != null)
                OnCollisionStayEvent(this, collision);
        }

        public void OnCollisionExit(Collision collision)
        {
            if (OnCollisionExitEvent != null)
                OnCollisionExitEvent(this, collision);
        }

    }

}