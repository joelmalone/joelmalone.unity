﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components.EventHelpers
{

    public class TriggerEventBroadcasterComponent : MonoBehaviour
    {

        public delegate void TriggerEventDelegate(Component receiver, Collider other);

        public event TriggerEventDelegate OnTriggerEnterEvent;
        public event TriggerEventDelegate OnTriggerStayEvent;
        public event TriggerEventDelegate OnTriggerExitEvent;

        public void OnTriggerEnter(Collider other)
        {
            if (OnTriggerEnterEvent != null)
                OnTriggerEnterEvent(this, other);
        }

        public void OnTriggerStay(Collider other)
        {
            if (OnTriggerStayEvent != null)
                OnTriggerStayEvent(this, other);
        }

        public void OnTriggerExit(Collider other)
        {
            if (OnTriggerExitEvent != null)
                OnTriggerExitEvent(this, other);
        }

    }

}