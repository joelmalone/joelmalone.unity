﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;

namespace JoelMalone.Unity.Components.EventHelpers
{

    public class MouseEventBroadcasterComponent : MonoBehaviour
    {

        public delegate void MouseEventDelegate(MouseEventBroadcasterComponent sender);

        public event MouseEventDelegate OnMouseDownEvent = delegate { };
        public event MouseEventDelegate OnMouseDragEvent = delegate { };
        public event MouseEventDelegate OnMouseEnterEvent = delegate { };
        public event MouseEventDelegate OnMouseExitEvent = delegate { };
        public event MouseEventDelegate OnMouseOverEvent = delegate { };
        public event MouseEventDelegate OnMouseUpEvent = delegate { };
        public event MouseEventDelegate OnMouseUpAsButtonEvent = delegate { };

        public void OnMouseDown()
        {
            OnMouseDownEvent(this);
        }

        public void OnMouseDrag()
        {
            OnMouseDragEvent(this);
        }

        public void OnMouseEnter()
        {
            OnMouseEnterEvent(this);
        }

        public void OnMouseExit()
        {
            OnMouseExitEvent(this);
        }

        public void OnMouseOver()
        {
            OnMouseOverEvent(this);
        }

        public void OnMouseUp()
        {
            OnMouseUpEvent(this);
        }

        public void OnMouseUpAsButton()
        {
            OnMouseUpAsButtonEvent(this);
        }

    }

}
