﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Uses LateUpdate() to update the gameobject's rotation to face the target object.
    /// </summary>
    public class RotateToFaceGameObjectComponent : MonoBehaviour
    {

        public Transform Target;

        public void LateUpdate()
        {
            if (Target)
            {
                transform.LookAt(Target);
            }
        }

    }

}