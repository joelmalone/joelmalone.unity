﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using JoelMalone.Unity.Attributes;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Place this in the centre of the gameplay area, so the SkyboxCameraComponent
    /// can use it to scene camera replicate movement in the skybox.
    /// </summary>
    public class SkyboxAnchorComponent : MonoBehaviour
    {
    }

}
