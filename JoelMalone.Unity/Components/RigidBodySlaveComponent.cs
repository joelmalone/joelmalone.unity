﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// This component will "slave" the attached gameobject to the specified RigidBody,
    /// mimicing it's position and determining rotation from velocity along the horizontal 
    /// plane.  Useful for attaching colliders, etc, to a rigidbody while still maintaining
    /// sensible forward/left/up vectors.
    /// </summary>
    public class RigidBodySlaveComponent : MonoBehaviour
    {

        public Rigidbody RigidBody;
        public float MinVelocityToUpdateRotation = 0.1f;

        public void LateUpdate()
        {
            if (RigidBody)
            {
                transform.position = RigidBody.position;
                transform.localScale = RigidBody.transform.localScale;
                var flatVelocity = RigidBody.velocity.WithY(0);
                if (flatVelocity.sqrMagnitude > MinVelocityToUpdateRotation * MinVelocityToUpdateRotation)
                    transform.rotation = Quaternion.LookRotation(flatVelocity);
            }
        }

    }
}