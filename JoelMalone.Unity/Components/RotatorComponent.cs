﻿using JoelMalone.Unity.Attributes;
using JoelMalone.Unity.UI.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using JoelMalone.Unity.UnityPatterns;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Automatically rotates the gameobject.
    /// </summary>
    public class RotatorComponent : MonoBehaviour
    {

        public Vector3 EulerAnglesPerSecond = new Vector3(0, 1, 0);

        public void Update()
        {
            transform.Rotate(EulerAnglesPerSecond * Time.deltaTime);
        }

    }

}
