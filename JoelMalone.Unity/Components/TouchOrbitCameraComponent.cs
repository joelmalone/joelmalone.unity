﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Attach to a camera to allow orbiting an object via touch and mouse.
    /// </summary>
    public class TouchOrbitCameraComponent : MonoBehaviour
    {

        public Camera Camera;
        public bool Mouse0Input = false;
        public bool Mouse1Input = true;
        public bool TouchInput = true;

        public GameObject Target;
        Vector3 _focus;

        /// <summary>
        /// If not specified, will be determined from initial distance to target
        /// </summary>
        public float OptionalDistance = 0;
        public float XSpeed = 120.0f;
        public float YSpeed = 60.0f;
        public float ZSpeed = 120.0f;

        public float AutoRotateX = 2;

        float x = 0;
        float y = 0;
        bool _mouseDown;
        Vector3 _mouseStart;

        public void Start()
        {
            // Attempt to determine settings from the camera
            if (Target)
                SetFocus(Target.transform.position);
        }

        /// <summary>
        /// Sets focus to the game object specified, using the collider bounds if a collider is present.
        /// </summary>
        /// <param name="focus"></param>
        public void SetFocus(GameObject gameObject)
        {
            var collider = gameObject.GetComponent<Collider>();
            if (collider)
            {
                SetFocus(collider.bounds);
            }
            else
            {
                SetFocus(gameObject.transform.position);
            }
        }

        /// <summary>
        /// Sets focus to the point specified.  Clears Target.
        /// </summary>
        /// <param name="focus"></param>
        public void SetFocus(Vector3 focus)
        {
            this.D("SetFocus({0}).", focus);

            Target = null;
            _focus = focus;

            // Set the orbit params to maintain the current camera position
            var toFocus = GetFocus() - transform.position;
            var rot = Quaternion.LookRotation(toFocus);
            var angles = rot.eulerAngles;
            x = angles.y;
            y = angles.x;
            OptionalDistance = toFocus.magnitude;
        }

        /// <summary>
        /// Positions the camera to frame the bounds specified and 
        /// focuses on the bounds' centre.  Clears Target.
        /// </summary>
        /// <param name="bounds"></param>
        public void SetFocus(Bounds bounds)
        {
            this.D("SetFocus({0}).", bounds);

            CameraExtensions.FocusCameraOnBounds(Camera, bounds, 0.5f);
            SetFocus(bounds.center);
        }

        Vector3 GetFocus()
        {
            if (Target == null)
                return _focus;
            else
                return Target.transform.position;
        }

        public void LateUpdate()
        {
            if (Mathf.Approximately(OptionalDistance, 0))
            {
                OptionalDistance = (GetFocus() - transform.position).magnitude;
                this.D("Determined auto distance of {0}.", OptionalDistance);
            }

            x += AutoRotateX;
            SetTransform();

            bool mouseInput = (Mouse0Input && Input.GetMouseButton(0))
                || (Mouse1Input && Input.GetMouseButton(1));

            if (mouseInput)
            {
                if (!_mouseDown)
                {
                    //this.D("Mouse down.");

                    _mouseStart = Input.mousePosition;
                    _mouseDown = true;
                }
                else
                {
                    //this.D("Mouse dragged.");

                    var delta = Input.mousePosition - _mouseStart;
                    OnDragged(delta * 0.02f);
                }
            }
            else
            {
                _mouseDown = false;

                if (TouchInput)
                {
                    foreach (var touch in Input.touches)
                    {
                        if (touch.phase == TouchPhase.Moved)
                        {
                            OnDragged(touch.deltaPosition);
                        }
                    }
                }
            }

            var scroll = Input.GetAxis("Mouse ScrollWheel");
            OptionalDistance -= scroll * ZSpeed;
        }

        void OnDragged(Vector2 delta)
        {
            //this.D("Dragged: {0}.", delta);

            x += delta.x * XSpeed * 0.02f;
            y -= delta.y * YSpeed * 0.02f;

            SetTransform();
        }

        void SetTransform()
        {
            var rotation = Quaternion.Euler(y, x, 0);
            var position = rotation * new Vector3(0, 0, -OptionalDistance) + GetFocus();

            transform.rotation = rotation;
            transform.position = position;
        }

    }

}