﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using System.Collections.Generic;
using JoelMalone.Unity.Components.Triggerables;

namespace JoelMalone.Unity.Components.Triggers
{

    /// <summary>
    /// Maintains a list of items the collider is currently trigger-colliding with.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class TriggeringListComponent : MonoBehaviour , IDebugTextSource
    {

        private readonly HashSet<Collider> _triggering = new HashSet<Collider>();
        public HashSet<Collider> Triggering { get { return _triggering; } }

        public void OnTriggerEnter(Collider collider)
        {
            _triggering.Add(collider);
        }

        public void OnTriggerExit(Collider collider)
        {
            _triggering.Remove(collider);
        }

        public void OnEnable()
        {
            StartCoroutine(PeriodicallyPurgeDisabledC());
        }

        protected IEnumerator PeriodicallyPurgeDisabledC()
        {
            // Check for objects that have vanished, e.g. got destroyed or 
            //  other reason, that we must manually remove from the list of 
            //  colliding colliders
            var wairForSeconds = new WaitForSeconds(1);
            while (enabled)
            {
                _triggering.RemoveWhere(c => c == null);
                yield return wairForSeconds;
            }
        }

        public string GetDebugText()
        {
            return string.Format("Triggering: {0}", _triggering.Count);
        }
    }

}
