﻿using UnityEngine;
using System.Collections;
using JoelMalone.Unity;
using System.Linq;
using System;
using System.Collections.Generic;
using JoelMalone.Unity.Components.Triggerables;

namespace JoelMalone.Unity.Components.Triggers
{

    /// <summary>
    /// A trigger that calls Trigger() when the first object enters the 
    /// collider and Untrigger() when all objects have left.
    /// Can use LayerFilter to restrict the set of relevant objects.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class ColliderTrigger : MonoBehaviour
    {

        public string TriggerName;

        public LayerMask LayerFilter = -1;

        private List<Collider> _colliding = new List<Collider>();

        public void OnTriggerEnter(Collider collider)
        {
            if (!LayerFilter.Includes(collider.gameObject.layer))
                return;

            var count = _colliding.Count;

            if (!_colliding.Contains(collider))
                _colliding.Add(collider);

            if(count == 0 && _colliding.Count > 0)
            {
                this.BroadcastTrigger(TriggerName);
            }
        }

        public void OnTriggerExit(Collider collider)
        {
            if (!LayerFilter.Includes(collider.gameObject.layer))
                return;

            var count = _colliding.Count;

            _colliding.Remove(collider);

            if (count > 0 && _colliding.Count == 0)
            {
                this.BroadcastUntrigger(TriggerName);
            }
        }

        public void Update()
        {
            // Check for objects that have vanished, e.g. got destroyed or 
            //  other reason, that we must manually remove from the list of 
            //  colliding colliders
            if(_colliding.Any(c => c == null))
            {
                _colliding = _colliding.Where(c => c != null).ToList();
                if (!_colliding.Any())
                    this.BroadcastUntrigger(TriggerName);
            }
        }

    }

}
