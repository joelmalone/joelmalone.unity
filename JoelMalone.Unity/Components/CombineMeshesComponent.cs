﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;

namespace Components
{

    /// <summary>
    /// This component will combine all MeshFilters in descendant objects,
    /// grouping meshes by their material.  The source meshes' GameObjects
    /// will be disabled.  The combined mesh will be placed in a sub-GameObject,
    /// named according to the material.  This effectively reduces draw calls.
    /// 
    /// To use this component, simply place it on the root GameObject that 
    /// contains child GameObjects with MeshFilters.
    /// 
    /// Note: does not support multiple materials within a mesh renderer - only 
    /// the first material is considered.
    /// </summary>
    public class CombineMeshesComponent : MonoBehaviour
    {

        public void Start()
        {
            var filtersByMaterial = from f in GetComponentsInChildren<MeshFilter>()
                                     let material = f.GetComponent<MeshRenderer>().sharedMaterial
                                     group f by material into g
                                     select g;

            var rootObject = new GameObject("_Combined Meshes");
            rootObject.transform.ParentAndReset(transform);

            foreach(var g in filtersByMaterial)
            {
                var name = string.Format("Mat: {0}", g.Key.name);
                GameObject go = new GameObject(name);
                go.transform.ParentAndReset(rootObject.transform);
                
                var combines = from f in g
                               select new CombineInstance
                               {
                                   mesh = f.sharedMesh,
                                   transform = f.transform.localToWorldMatrix,
                               };

                var combinedFilter = go.AddComponent<MeshFilter>();
                combinedFilter.mesh = new Mesh();
                combinedFilter.mesh.CombineMeshes(combines.ToArray());

                var combinedRenderer = go.AddComponent<MeshRenderer>();
                combinedRenderer.material = g.Key;

                foreach(var filter in g)
                {
                    filter.gameObject.SetActive(false);
                }
            }


            //var combine = new CombineInstance[meshFilters.Length];
            //Material material = null;
            //for (var i = 0; i < meshFilters.Length; i++)
            //{
            //    combine[i].mesh = meshFilters[i].sharedMesh;
            //    combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            //    meshFilters[i].gameObject.SetActive(false);

            //    var thisMaterial = meshFilters[i].GetComponent<Renderer>().material;
            //    if (material == null)
            //        material = thisMaterial;
            //    else if (thisMaterial != material)
            //        this.W("More than one material found when combining meshes.");
            //}
            //transform.GetComponent<MeshFilter>().mesh = new Mesh();
            //transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            //transform.gameObject.SetActive(true);
        }

    }
}
