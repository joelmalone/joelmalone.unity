﻿using System;
using System.Collections;
using UnityEngine;

namespace JoelMalone.Unity.Components
{
    
    /// <summary>
    /// Coroutune executor that will execute a single coroutine, calling Dispose()
    /// on it if it is aborted before it can complete execution.
    /// This is handy for times when you need try..finally or using() 
    /// blocks to execute within your coroutines, e.g. to perform 
    /// cleanup.
    /// Note: does not explicitly call Dispose() when a coroutine
    /// runs to completion, though try..finally and using() blocks
    /// would have already executed anyway, in this situation.
    /// To use this class, call extension method StartDisposableCoroutine()
    /// on the component class; the coroutine itself will be executed on a 
    /// self-managed child gameobject.
    /// </summary>
    public class DisposableCoroutineExecutorComponent : MonoBehaviour
    {

        private IEnumerator _disposableCoroutine;

        public void StartDisposableCoroutine(IEnumerator coroutine)
        {
            if (_disposableCoroutine != null)
            {
                var disp = _disposableCoroutine as IDisposable;
                if (disp != null)
                    disp.Dispose();
                StopAllCoroutines();
            }

            _disposableCoroutine = coroutine;

            if (_disposableCoroutine != null)
                StartCoroutine(_disposableCoroutine);
        }

    }

    public static class DisposableCoroutineExecutorComponentExtensions
    {

        public static void StartDisposableCoroutine(this Component me, IEnumerator coroutine)
        {
            var executor = me.GetComponent<DisposableCoroutineExecutorComponent>();
            if(!executor)
                executor = me.gameObject.AddComponent<DisposableCoroutineExecutorComponent>();
            
            executor.StartDisposableCoroutine(coroutine);
        }

    }

}
