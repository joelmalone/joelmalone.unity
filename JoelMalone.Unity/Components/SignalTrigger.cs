﻿using JoelMalone.Unity.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using JoelMalone.Unity;
using System.Collections;
using JoelMalone.Unity.UnityPatterns;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Components
{

    /// <summary>
    /// Upon something entering the trigger collider, calls Entered() on 
    /// the target trigger and Exited() when something leaves.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class SignalTrigger : MonoBehaviour
    {

        public Signal Target;

        public void OnTriggerEnter(Collider collider)
        {
            if (Target != null)
                Target.Entered();
            else
                this.W("No target has been set for the trigger!");
        }

        public void OnTriggerExit(Collider collider)
        {
            if (Target != null)
                Target.Exited();
            else
                this.W("No target has been set for the trigger!");
        }

    }

}
