﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity
{
 
    /// <summary>
    /// Quick-and-dirty class that executes an when disposed.
    /// </summary>
    public class DisposeAction : IDisposable
    {
        public Action Action { get ;set; }
        public bool Disposed { get; internal set; }

        public void Dispose()
        {
            if (Action != null)
                Action();
            Disposed = true;
        }

    }

}
