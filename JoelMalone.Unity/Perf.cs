﻿using System;
using System.Diagnostics;
using JoelMalone.Logging;

namespace JoelMalone.Unity
{

    /// <summary>
    /// Contains a bunch of static methods for keeping tabs on the performance of
    /// your application throughout the course of development.
    /// </summary>
    public static class Perf
    {

        /// <summary>
        /// Times the code executed within a using block, writing 
        /// the timing as a verbose entry to the log.
        /// </summary>
        /// <param name="context">The object executing the code.</param>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable Timed(object context, string tag)
        {
            var sw = new Stopwatch();
            var disposable = new DisposeAction
            {
                Action = () =>
                {
                    sw.Stop();
                    Log.Write(context, LogLevel.Verbose, "\"{0}\" took {1:#,##0} msec.", tag, sw.ElapsedMilliseconds);
                }
            };
            sw.Start();
            return disposable;
        }

        /// <summary>
        /// Times the code executed within a using block, writing 
        /// the timing as a verbose entry to the log.
        /// </summary>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable Timed(string tag)
        {
            var sw = new Stopwatch();
            var disposable = new DisposeAction
            {
                Action = () =>
                {
                    sw.Stop();
                    Log.Write(null, LogLevel.Verbose, "\"{0}\" took {1:#,##0} msec.", tag, sw.ElapsedMilliseconds);
                }
            };
            sw.Start();
            return disposable;
        }

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds the specified time limit.
        /// </summary>
        /// <param name="context">The object executing the code.</param>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <param name="timeLimitMsec">The permitted amount of time.</param>
        /// <returns></returns>
        public static IDisposable Policed(object context, string tag, long timeLimitMsec)
        {
            var sw = new Stopwatch();
            var disposable = new DisposeAction
            {
                Action = () =>
                {
                    sw.Stop();
                    if (sw.ElapsedMilliseconds > timeLimitMsec)
                        Log.Write(context, LogLevel.Warning, "Performance warning: \"{0}\" took {1:#,##0} msec, limit was {2:#,##0}.", tag, sw.ElapsedMilliseconds, timeLimitMsec);
                }
            };
            sw.Start();
            return disposable;
        }

        /// <summary>
        /// Permitted amount of time to allow something to run in a 'realtime' 
        /// scenario, e.g. none-to-minimal performance impact.  Intended for
        /// things that may get run every frame.
        /// </summary>
        public const long UiRealtimeLimit = 1;

        /// <summary>
        /// Permitted amount of time to allow something to run in a 'UI-realtime' 
        /// scenario, e.g. minimal-to-mediam performance impact - a one-frame
        /// hiccup is OK (based on 60fps).  Intended for one-shot things, e.g. 
        /// a button press.
        /// </summary>
        public const long UiSlowtimeLimit = 16;

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds a predefined time limit.
        /// </summary>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable PolicedUiRealtime(string tag)
        {
            return Policed(null, tag, Perf.UiRealtimeLimit);
        }

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds a predefined time limit.
        /// </summary>
        /// <param name="context">The object executing the code.</param>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable PolicedUiRealtime(object context, string tag)
        {
            return Policed(context, tag, Perf.UiRealtimeLimit);
        }

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds a predefined time limit.
        /// </summary>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable PolicedUiSlowtime(string tag)
        {
            return Policed(null, tag, Perf.UiSlowtimeLimit);
        }

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds a predefined time limit.
        /// </summary>
        /// <param name="context">The object executing the code.</param>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <returns></returns>
        public static IDisposable PolicedUiSlowtime(object context, string tag)
        {
            return Policed(context, tag, Perf.UiSlowtimeLimit);
        }

        /// <summary>
        /// Times the code executed within a using block, writing a warning to 
        /// the log if it exceeds the specified time limit.
        /// </summary>
        /// <param name="tag">A human-readable label for the code being executed.</param>
        /// <param name="timeLimitMsec">The permitted amount of time.</param>
        /// <returns></returns>
        public static IDisposable Policed(string tag, long timeLimitMsec)
        {
            return Policed(null, tag, timeLimitMsec);
        }

    }

}
