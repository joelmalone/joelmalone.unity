﻿using System;

namespace JoelMalone.Unity
{

    public interface IEventTrigger
    {
        void Fire();
    }

    public interface IEvent
    {
        event Action OnFired;
    }

    public class Event : IEvent, IEventTrigger
    {

        public event Action OnFired;

        public void Fire()
        {
            if (OnFired != null)
                OnFired();
        }
    }



    public interface IEventTrigger<TParam>
    {
        void Fire(TParam param);
    }

    public interface IEvent<TParam>
    {
        event Action<TParam> OnFired;
    }

    public class Event<TParam> : IEvent<TParam>, IEventTrigger<TParam>
    {

        public event Action<TParam> OnFired;

        public void Fire(TParam param)
        {
            if(OnFired != null)
                OnFired(param);
        }
    }



    public interface IEventTrigger<TParam1, TParam2>
    {
        void Fire(TParam1 param1, TParam2 param2);
    }

    public interface IEvent<TParam1, TParam2>
    {
        event Action<TParam1, TParam2> OnFired;
    }

    public class Event<TParam1, TParam2> : IEvent<TParam1, TParam2>, IEventTrigger<TParam1, TParam2>
    {

        public event Action<TParam1, TParam2> OnFired;

        public void Fire(TParam1 param1, TParam2 param2)
        {
            if(OnFired != null)
                OnFired(param1, param2);
        }
    }



    public interface IEventTrigger<TParam1, TParam2, TParam3>
    {
        void Fire(TParam1 param1, TParam2 param2, TParam3 param3);
    }

    public interface IEvent<TParam1, TParam2, TParam3>
    {
        event Action<TParam1, TParam2, TParam3> OnFired;
    }

    public class Event<TParam1, TParam2, TParam3> : IEvent<TParam1, TParam2, TParam3>, IEventTrigger<TParam1, TParam2, TParam3>
    {

        public event Action<TParam1, TParam2, TParam3> OnFired;

        public void Fire(TParam1 param1, TParam2 param2, TParam3 param3)
        {
            if(OnFired != null)
                OnFired(param1, param2, param3);
        }
    }

}
