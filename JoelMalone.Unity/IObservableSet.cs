﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace JoelMalone.Unity
//{

//    public interface IReadOnlyObservableSet<T> : IEnumerable<T>
//    {
//        int Count { get; }
//        bool Contains(T item);
//        IEqualityComparer<T> Comparer { get; }
//    }

//    public interface IObservableSetActions<T> : IReadOnlyObservableSet<T>
//    {
//        void AddRange(IEnumerable<T> items);
//        void Remove(T item);
//        void RemoveWhere(Func<T, bool> predicate);
//        void InvalidateRange(IEnumerable<T> items);
//        void Clear();
//    }

//    public interface IObservableSetEvents<T>
//    {
//        event Action<ICollection<T>> OnItemsAdded;
//        event Action<ICollection<T>> OnItemsRemoved;
//        event Action<ICollection<T>> OnItemsInvalidated;
//    }

//    public interface IObservableSet<T> : IReadOnlyObservableSet<T>, IObservableSetActions<T>, IObservableSetEvents<T>
//    { }

//    public static class ObservableSetFactory
//    {

//        public static IObservableSet<T> Create<T>()
//        {
//            return new ObservableHashSet<T>(new HashSet<T>());
//        }

//        public static IObservableSet<T> Create<T>(IEnumerable<T> items)
//        {
//            return new ObservableHashSet<T>(new HashSet<T>(items));
//        }

//        public static IObservableSet<T> Create<T>(IEqualityComparer<T> equalityComparer)
//        {
//            return new ObservableHashSet<T>(new HashSet<T>(equalityComparer));
//        }

//        public static IObservableSet<T> Create<T>(IEnumerable<T> items, IEqualityComparer<T> equalityComparer)
//        {
//            return new ObservableHashSet<T>(new HashSet<T>(items, equalityComparer));
//        }

//    }

//    class ObservableHashSet<T> : IObservableSet<T>
//    {

//        private readonly HashSet<T> _set;

//        public event Action<ICollection<T>> OnItemsAdded;
//        public event Action<ICollection<T>> OnItemsRemoved;
//        public event Action<ICollection<T>> OnItemsInvalidated;

//        public ObservableHashSet(HashSet<T> set)
//        {
//            _set = set;
//        }

//        public int Count { get { return _set.Count; } }

//        public bool Contains(T item)
//        {
//            return _set.Contains(item);
//        }

//        public IEnumerator<T> GetEnumerator()
//        {
//            return _set.GetEnumerator();
//        }

//        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
//        {
//            return _set.GetEnumerator();
//        }

//        public void AddRange(IEnumerable<T> items)
//        {
//            var newItems = new HashSet<T>(items);
//            newItems.ExceptWith(_set);

//            if (newItems.Count == 0)
//                return;

//            _set.UnionWith(newItems);

//            if (OnItemsAdded != null)
//                OnItemsAdded(newItems);
//        }

//        public void Remove(T item)
//        {
//            if (_set.Remove(item) && OnItemsRemoved != null)
//            {
//                OnItemsRemoved(new[] { item });
//            }
//        }

//        public void RemoveWhere(Func<T, bool> predicate)
//        {
//            var removedItems = (
//                from i in _set
//                where predicate(i) && _set.Remove(i)
//                select i)
//                .ToList();

//            if (removedItems.Count > 0 && OnItemsRemoved != null)
//                OnItemsRemoved(removedItems);
//        }

//        public void InvalidateRange(IEnumerable<T> items)
//        {
//            if(OnItemsInvalidated == null)
//                return;

//            var invalidatedItems = new HashSet<T>(items, _set.Comparer);
//            invalidatedItems.IntersectWith(_set);

//            if (invalidatedItems.Count > 0)
//                OnItemsInvalidated(invalidatedItems);
//        }

//        public void Clear()
//        {
//            if (_set.Count == 0)
//                return;

//            var itemsRemoved = _set.ToList();
//            _set.Clear();

//            if (OnItemsRemoved != null)
//                OnItemsRemoved(itemsRemoved);
//        }

//        public IEqualityComparer<T> Comparer { get { return _set.Comparer; } }

//    }

//    public static class ObservableSetHelpers
//    {

//        public static void Add<T>(this IObservableSetActions<T> me, params T[] items)
//        {
//            me.AddRange(items);
//        }

//        public static void RemoveRange<T>(this IObservableSetActions<T> me, IEnumerable<T> items)
//        {
//            var set = new HashSet<T>(items);
//            if (set.Count > 0)
//                me.RemoveWhere(set.Contains);
//        }

//        /// <summary>
//        /// Binds to the events of another display set, replicating list changes in the local set.  Dispose the returned
//        /// IDisposable to unbind, or call UnbindFrom().  The current set of items from the bound set will be added immediately.
//        /// </summary>
//        /// <typeparam name="TSource"></typeparam>
//        /// <param name="me"></param>
//        /// <param name="other"></param>
//        /// <returns></returns>
//        public static IDisposable BindTo<TSource>(this IObservableSetActions<TSource> me, IObservableSet<TSource> other)
//        {
//            me.AddRange(other);

//            other.OnItemsAdded += me.AddRange;
//            other.OnItemsInvalidated += me.InvalidateRange;
//            other.OnItemsRemoved += me.RemoveRange;

//            return new DisposeAction
//            {
//                Action = () => UnbindFrom(me, other)
//            };
//        }

//        /// <summary>
//        /// Unbind from the other set.  The current set of items in the other set will be removed immediately.
//        /// </summary>
//        /// <typeparam name="TSource"></typeparam>
//        /// <param name="me"></param>
//        /// <param name="other"></param>
//        public static void UnbindFrom<TSource>(this IObservableSetActions<TSource> me, IObservableSet<TSource> other)
//        {
//            other.OnItemsAdded -= me.AddRange;
//            other.OnItemsInvalidated -= me.InvalidateRange;
//            other.OnItemsRemoved -= me.RemoveRange;

//            me.RemoveRange(other);
//        }

//        public static IDisposable Subscribe<T>(this IObservableSetEvents<T> me, Action<ICollection<T>> onAdded, Action<ICollection<T>> onInvalidated, Action<ICollection<T>> onRemoved)
//        {
//            me.OnItemsAdded += onAdded;
//            me.OnItemsInvalidated += onInvalidated;
//            me.OnItemsRemoved += onRemoved;

//            return new DisposeAction
//            {
//                Action = () =>
//                {
//                    me.OnItemsAdded -= onAdded;
//                    me.OnItemsInvalidated -= onInvalidated;
//                    me.OnItemsRemoved -= onRemoved;
//                },
//            };
//        }

//    }

//}
