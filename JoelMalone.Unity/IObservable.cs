﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace JoelMalone.Unity
//{

//    public interface IObservableEvents<T>
//    {
//        event Action<T> OnChanged;
//    }

//    public interface IReadonlyObservable<T> : IObservableEvents<T>
//    {
//        T Value { get; }
//    }

//    public interface IObservableActions<T>
//    {
//        void SetValue(T value);
//    }

//    public interface IObservable<T> : IReadonlyObservable<T>, IObservableActions<T>, IObservableEvents<T>
//    {
//    }

//    public class Observable<T> : IObservable<T>
//    {

//        private static readonly IEqualityComparer<T> _comparer = EqualityComparer<T>.Default;
//        private T _value;

//        public event Action<T> OnChanged;

//        public T Value
//        {
//            get { return _value; }
//            set
//            {
//                if (_comparer.Equals(_value, value))
//                    return;

//                _value = value;
//                if (OnChanged != null)
//                    OnChanged(_value);
//            }
//        }

//        void IObservableActions<T>.SetValue(T value)
//        {
//            Value = value;
//        }


//    }

//}
