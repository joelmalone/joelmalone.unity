﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity
{
    
    /// <summary>
    /// Unused interface for allowing objects of 
    /// any type to define debug text, for example, 
    /// for dislaying in debug UIs or logs.
    /// </summary>
    public interface IDebugTextSource
    {
        string GetDebugText();
    }

}
