﻿/*
From: "Expression Parsing and Nested Properties"
URL: http://www.codeproject.com/Articles/733296/Expression-Parsing-and-Nested-Properties
Author: "n.podbielski"
License: The Code Project Open License (CPOL) 1.02
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace JoelMalone.Unity
{

    public static class ExpressionOperator
    {
        public static MemberExpression GetMemberExpression(Expression expression)
        {
            if (expression is MemberExpression)
            {
                return (MemberExpression)expression;
            }
            else if (expression is LambdaExpression)
            {
                var lambdaExpression = expression as LambdaExpression;
                if (lambdaExpression.Body is MemberExpression)
                {
                    return (MemberExpression)lambdaExpression.Body;
                }
                else if (lambdaExpression.Body is UnaryExpression)
                {
                    return ((MemberExpression)((UnaryExpression)lambdaExpression.Body).Operand);
                }
            }
            return null;
        }

        public static string GetPropertyPath(Expression expr)
        {
            var path = new StringBuilder();
            MemberExpression memberExpression = GetMemberExpression(expr);
            do
            {
                if (path.Length > 0)
                {
                    path.Insert(0, ".");
                }
                path.Insert(0, memberExpression.Member.Name);
                memberExpression = ExpressionOperator.GetMemberExpression(memberExpression.Expression);
            }
            while (memberExpression != null);
            return path.ToString();
        }
    }

    public static class ExpressionOperatorHelpers
    {
        //public static object GetPropertyValue(this object obj, string propertyPath)
        //{
        //    object propertyValue = null;
        //    if (propertyPath.IndexOf(".") < 0)
        //    {
        //        var objType = obj.GetType();
        //        propertyValue = objType.GetProperty(propertyPath).GetValue(obj, null);
        //        return propertyValue;
        //    }
        //    var properties = propertyPath.Split('.').ToList();
        //    var midPropertyValue = obj;
        //    while (properties.Count > 0)
        //    {
        //        var propertyName = properties.First();
        //        properties.Remove(propertyName);
        //        propertyValue = midPropertyValue.GetPropertyValue(propertyName);
        //        midPropertyValue = propertyValue;
        //    }
        //    return propertyValue;
        //}

        public static string GetPropertyPath<TObj, TRet>(this Expression<Func<TObj, TRet>> expr)
        {
            return ExpressionOperator.GetPropertyPath(expr);
        }

    }
}
