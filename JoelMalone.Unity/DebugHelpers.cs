﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    /// <summary>
    /// Helper methods for writing debug code or to assist with debugging.
    /// </summary>
    public static class DebugHelpers
    {

        public static IEnumerable<Vector3> GetGridLines(Vector3 origin, Vector3 x, Vector3 y, int sideCount)
        {
            for (int i = 0; i <= sideCount; i++)
                for (int j = 0; j <= sideCount; j++)
                {
                    var v1 = x * i - x * (sideCount * 0.5f) - y * (sideCount * 0.5f);
                    var v2 = x * i - x * (sideCount * 0.5f) + y * (sideCount * 0.5f);

                    yield return origin + v1;
                    yield return origin + v2;

                    v1 = -x * (sideCount * 0.5f) + j * y - y * (sideCount * 0.5f);
                    v2 = x * (sideCount * 0.5f) + j * y - y * (sideCount * 0.5f);

                    yield return origin + v1;
                    yield return origin + v2;
                }
        }
    
    }
}
