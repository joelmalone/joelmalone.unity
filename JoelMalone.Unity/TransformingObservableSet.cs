﻿//using System;
//using System.Linq;
//using System.Collections.Generic;

//namespace JoelMalone.Unity
//{

//    /// <summary>
//    /// An implementation of IObservableSet that contains a set of TSource objects, but presents
//    /// events in the form of TTransformed objects, using the transformation methods supplied.
//    /// </summary>
//    /// <typeparam name="TSource"></typeparam>
//    /// <typeparam name="TTransformed"></typeparam>
//    public class TransformingObservableSet<TSource, TTransformed> : IObservableSetActions<TSource>, IObservableSetEvents<TTransformed>, IDisposable
//    {

//        private readonly Func<TSource, TTransformed> _transformFunc;
//        private readonly Action<TSource, TTransformed> _invalidateAction;
//        private readonly Dictionary<TSource, TTransformed> _dictionary;
//        private readonly ReadOnlyDictionary<TSource, TTransformed> _readonlyDictionary;

//        public event Action<ICollection<TTransformed>> OnItemsAdded;
//        public event Action<ICollection<TTransformed>> OnItemsInvalidated;
//        public event Action<ICollection<TTransformed>> OnItemsRemoved;

//        public TransformingObservableSet(Func<TSource, TTransformed> transformFunc, Action<TSource, TTransformed> invalidateAction)
//            : this(transformFunc, invalidateAction, EqualityComparer<TSource>.Default)
//        { }

//        public TransformingObservableSet(Func<TSource, TTransformed> transformFunc, Action<TSource, TTransformed> invalidateAction, IEqualityComparer<TSource> comparer)
//        {
//            _transformFunc = transformFunc;
//            _invalidateAction = invalidateAction;
//            _dictionary = new Dictionary<TSource, TTransformed>(comparer);
//            _readonlyDictionary = new ReadOnlyDictionary<TSource, TTransformed>(_dictionary);
//        }

//        public void Dispose()
//        {
//            Clear();
//        }

//        public ReadOnlyDictionary<TSource, TTransformed> Dictionary { get { return _readonlyDictionary; } }
        
//        public IEqualityComparer<TSource> Comparer { get { return _dictionary.Comparer; } }

//        public void AddRange(IEnumerable<TSource> items)
//        {
//            var list = new List<TTransformed>();

//            foreach (var i in items)
//            {
//                var transformed = _transformFunc(i);
//                _dictionary[i] = transformed;

//                list.Add(transformed);
//            }

//            if (OnItemsAdded != null)
//                OnItemsAdded(list);
//        }

//        public void Set(IEnumerable<TSource> items)
//        {
//            var itemsSet = new HashSet<TSource>(items);

//            // Remove source items and build a list of transformed items 
//            //  to be removed
//            var transformedRemoved = (
//                from e in _dictionary.ToList()
//                where !itemsSet.Contains(e.Key)
//                let removed = _dictionary.Remove(e.Key) // Return value ignored
//                select e.Value).ToList();

//            // Ignore existing items in the incoming set as we don't need to do 
//            //  anything with them.  itemsSet now becomes the set of new items
//            itemsSet.ExceptWith(_dictionary.Keys);

//            // Create the new items and build a list of added items to broadcast
//            var transformedAdded = new List<TTransformed>(itemsSet.Count);
//            foreach (var i in itemsSet)
//            {
//                var transformed = _transformFunc(i);
//                _dictionary[i] = transformed;

//                transformedAdded.Add(transformed);
//            }

//            // Broadcast events
//            if (OnItemsRemoved != null)
//                OnItemsRemoved(transformedRemoved);
//            if (OnItemsAdded != null)
//                OnItemsAdded(transformedAdded);
//        }

//        public void Remove(TSource item)
//        {
//            var transformed = _dictionary.TryGetOrNull(item);
//            if(_dictionary.Remove(item) && OnItemsRemoved != null)
//                OnItemsRemoved(new[] { transformed });
//        }

//        public void RemoveWhere(Func<TSource, bool> predicate)
//        {
//            // TODO: the ToList() is a hack tofix a sync error caused by 
//            //  remove items while during enumeration; a real fix may
//            //  be necessary if the ToList() causes too much gc
//            var transformedsToRemove = (
//                from i in _dictionary.ToList()
//                where predicate(i.Key) && _dictionary.Remove(i.Key)
//                select i.Value
//                ).ToList();

//            //JoelMalone.Logging.Log.Write(this, Logging.LogLevel.Verbose, "Removing {0} items.", transformedsToRemove.Count);

//            if (transformedsToRemove.Count > 0 && OnItemsRemoved != null)
//                OnItemsRemoved(transformedsToRemove);
//        }

//        public void InvalidateRange(IEnumerable<TSource> items)
//        {
//            var list = new List<TTransformed>();

//            foreach (var i in items)
//            {
//                var transformed = _dictionary.TryGetOrNull(i);

//                if (transformed != null)
//                {
//                    _invalidateAction(i, transformed);
//                    list.Add(transformed);
//                }
//            }

//            if (OnItemsInvalidated != null)
//                OnItemsInvalidated(list);
//        }

//        public void Clear()
//        {
//            var transformedToRemove = (
//                from transformed in _dictionary.Values
//                where transformed != null
//                select transformed
//                ).ToList();

//            if (OnItemsRemoved != null)
//                OnItemsRemoved(transformedToRemove);

//            _dictionary.Clear();
//        }

//        public int Count { get { return _dictionary.Count; } }

//        public bool Contains(TSource item)
//        {
//            return _dictionary.ContainsKey(item);
//        }

//        public IEnumerator<TSource> GetEnumerator()
//        {
//            return _dictionary.Keys.GetEnumerator();
//        }

//        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
//        {
//            return _dictionary.Keys.GetEnumerator();
//        }
//    }

//    public static class TransformingObservableSetHelpers
//    {

//    }

//}
