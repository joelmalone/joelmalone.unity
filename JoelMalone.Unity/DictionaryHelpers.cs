﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace JoelMalone.Unity
{

    public static class DictionaryHelpers
    {

        //public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<Tuple<TKey, TValue>> pairs)
        //{
        //    var dictionary = new Dictionary<TKey, TValue>();
        //    foreach (var pair in pairs)
        //        dictionary.Add(pair.Value1, pair.Value2);
        //    return dictionary;
        //}

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> pairs)
        {
            return pairs.ToDictionary(null);
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> pairs, IEqualityComparer<TKey> comparer)
        {
            var dictionary = new Dictionary<TKey, TValue>(comparer);
            foreach (var pair in pairs)
                dictionary.Add(pair.Key, pair.Value);
            return dictionary;
        }

        public static TValue TryGetOrNull<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue result;
            dictionary.TryGetValue(key, out result);
            return result;
        }

        public static IEnumerable<TValue> TryGetRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys)
        {
            return
                from key in keys
                let value = dictionary.TryGetOrNull(key)
                where value != null
                select value;
        }

    }

}
