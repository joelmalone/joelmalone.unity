﻿using System;

namespace JoelMalone.Unity
{

    /// <summary>
    /// Holds a single value, invoking Actions when the value changes.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class SerialValue<TValue>
        where TValue : class
    {

        private readonly Func<TValue, TValue, bool> _equalityComparer;
        private TValue _value;

        /// <summary>
        /// Called when Value has changed to a non-null value.
        /// </summary>
        public Action<TValue> OnSetValue { get; set; }
        /// <summary>
        /// Called when Value has changed to a null value.
        /// </summary>
        public Action OnSetNullValue { get; set; }
        /// <summary>
        /// Called when the current (non-null) Value is about to be replaced.
        /// </summary>
        public Action<TValue> OnReleaseValue { get; set; }

        public SerialValue(Func<TValue, TValue, bool> equalityComparer)
        {
            _equalityComparer = equalityComparer == null ? object.ReferenceEquals : equalityComparer;
        }

        public SerialValue()
            : this(null)
        { }

        public SerialValue(Action<TValue> onSetValue, Action<TValue> onReleaseValue)
            : this(null)
        {
            OnSetValue = onSetValue;
            OnReleaseValue = onReleaseValue;
        }

        public TValue Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (_equalityComparer(_value, value))
                    return;

                if (_value != null && OnReleaseValue != null)
                    OnReleaseValue(_value);

                _value = value;

                if (OnSetValue != null)
                {
                    if (_value != null)
                    {
                        if (OnSetValue != null)
                            OnSetValue(_value);
                    }
                    else
                    {
                        if (OnSetNullValue != null)
                            OnSetNullValue();
                    }
                }
            }
        }

    }

}
