﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity
{

    public static class SelectorEqualityComparer
    {

        public static SelectorEqualityComparer<T, TProperty> Create<T, TProperty>(Func<T, TProperty> propertySelector)
        {
            return new SelectorEqualityComparer<T, TProperty>(propertySelector);
        }

    }

    public class SelectorEqualityComparer<T, TProperty> : IEqualityComparer<T>
    {

        private readonly Func<T, TProperty> _propertySelector;

        public SelectorEqualityComparer(Func<T, TProperty> propertySelector)
        {
            _propertySelector = propertySelector;
        }

        public bool Equals(T x, T y)
        {
            if (ReferenceEquals(x, y))
                return true;

            var prop1 = _propertySelector(x);
            var prop2 = _propertySelector(y);
            return Comparer.Equals(prop1, prop2);
        }

        public int GetHashCode(T obj)
        {
            var prop = _propertySelector(obj);
            return prop.GetHashCode();
        }

    }

}
