﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity.State
{
    
    public class ActionState : IState
    {

        public Action OnBegin { get; set; }
        public Action OnEnd { get; set; }

        void IState.OnBegin()
        {
            if (OnBegin != null)
                OnBegin();
        }

        void IState.OnEnd()
        {
            if (OnEnd != null)
                OnEnd();
        }
    }

}
