﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity.State
{
    
    public interface IState
    {
        void OnBegin();
        void OnEnd();
    }

}
