﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity.State
{

    public class SerialState<TState>
        where TState : class, IState
    {
        private IState _current;

        public IState Current
        {
            get
            {
                return _current;
            }
            set
            {
                if (_current == value)
                    return;

                if (_current != null)
                    _current.OnEnd();

                _current = value;

                if (_current != null)
                    _current.OnBegin();
            }
        }

    }

}
