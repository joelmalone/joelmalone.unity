﻿using UnityEngine;
using System.Collections;

namespace JoelMalone.Unity.UnityPatterns
{

    [System.Serializable]
    public class Signal
    {
        public GameObject target;
        public string enterMethod;
        public string enterMethodArgType;
        public string exitMethod;
        public string exitMethodArgType;

        public Signal()
        {

        }
        public Signal(System.Type argType)
        {
            this.enterMethodArgType = argType.FullName;
        }

        public void Entered()
        {
            Invoke(target, enterMethod);
        }
        public void Entered(object value)
        {
            Invoke(target, enterMethod, enterMethodArgType, value);
        }

        public void Exited()
        {
            Invoke(target, exitMethod);
        }
        public void Exited(object value)
        {
            Invoke(target, exitMethod, exitMethodArgType, value);
        }

        protected static void Invoke(GameObject target, string method)
        {
            if (target != null && !string.IsNullOrEmpty(method))
                target.SendMessage(method, SendMessageOptions.RequireReceiver);
        }
        protected static void Invoke(GameObject target, string method, string argType, object value)
        {
            if (argType != null)
            {
                if (target != null && !string.IsNullOrEmpty(method))
                {
                    if (argType.Equals(value.GetType().FullName))
                        target.SendMessage(method, value, SendMessageOptions.RequireReceiver);
                    else
                        Debug.LogError("Incorrect parameter type, expected [" + argType + "], got [" + value.GetType().FullName + "].");
                }
            }
            else
                Invoke(target, method);
        }
    }

    public class SignalAttribute : System.Attribute
    {
        public string name;

        public SignalAttribute()
        {

        }
        public SignalAttribute(string name)
        {
            this.name = name;
        }
    }

}