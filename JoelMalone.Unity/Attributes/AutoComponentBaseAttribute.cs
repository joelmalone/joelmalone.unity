﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{

    [AttributeUsage(AttributeTargets.Field)]
    public abstract class AutoComponentBaseAttribute : BootstrapAttribute
    {

        public bool IncludeInactiveGameObjects { get; set; }
        public bool IncludeDisabledBehaviours { get; set; }

        public override void Execute(object instance, MemberInfo memberInfo)
        {
            var unityInstance = instance as Component;
            if (unityInstance == null)
                throw new AutoAttributeException("Attribute can only be applied to fields within a class derived from Component.");

            FieldInfo fieldInfo = (FieldInfo)memberInfo;

            var fieldType = fieldInfo.FieldType;
            if (fieldType.IsArray)
            {
                // We are populating an array

                var elementType = fieldType.GetElementType();

                var foundComponents = ExecuteSearch(unityInstance, memberInfo, elementType);
                foundComponents = ApplyFilters(foundComponents, IncludeInactiveGameObjects, IncludeDisabledBehaviours);

                var typecastResult = Array.CreateInstance(elementType, foundComponents.Length);
                Array.Copy(foundComponents, typecastResult, foundComponents.Length);

                fieldInfo.SetValue(instance, typecastResult);
            }
            else
            {
                // We are populating a single item

                var foundComponents = ExecuteSearch(unityInstance, memberInfo, fieldType);
                foundComponents = ApplyFilters(foundComponents, IncludeInactiveGameObjects, IncludeDisabledBehaviours);

                if (foundComponents.Length > 1)
                    throw new AutoAttributeException("Unable to populate field {0} on component {1} because more than one child of type {2} was found.  If this is correct, you can change the type of the field to a type compatible with IList<{2}>.", memberInfo.Name, instance, fieldType.Name);
                else if (foundComponents.Length == 0)
                    throw new AutoAttributeException("Unable to populate field {0} on component {1} because no children component of type {2} was found.", memberInfo.Name, instance, fieldType.Name);

                var foundComponent = foundComponents.SingleOrDefault();

                fieldInfo.SetValue(instance, foundComponent);
            }

        }

        private static Component[] ApplyFilters(Component[] foundComponents, bool includeInactiveGameObjects, bool includeDisabledBehaviours)
        {
            if (includeDisabledBehaviours && includeInactiveGameObjects)
                // No work to be done
                return foundComponents;

            return (
                from c in foundComponents
                where includeInactiveGameObjects || c.gameObject.activeSelf
                let behaviour = c as Behaviour
                where includeDisabledBehaviours || behaviour == null || behaviour.enabled
                select c
                ).ToArray();
        }

        /// <summary>
        /// Subclasses use this to perform the search for matching components.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="elementType"></param>
        /// <returns></returns>
        protected abstract Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType);

    }

}
