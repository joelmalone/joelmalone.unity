﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class RequiredAttribute : BootstrapAttribute
    {

        public override void Execute(object obj, MemberInfo memberInfo)
        {
            FieldInfo fieldInfo = (FieldInfo)memberInfo;
            if (fieldInfo.GetValue(obj) == null)
                throw new AutoAttributeException("Object {0} is missing value for {1}.", obj, memberInfo.Name);
        }
    }

}
