﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using JoelMalone.Logging;

namespace JoelMalone.Unity.Attributes
{
    
    public static class Extensions
    {

        public static void ExecuteBootstrapAttributes(this object me)
        {
            var exceptionCount = 0;

            var membersToBootstrap = from p in me.GetType().GetMembers(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)
                                     let attributes = GetAttributes(p)
                               where attributes.Any()
                               select new { Property = p, Attributes = attributes };

            foreach (var i in membersToBootstrap)
            {
                foreach (var a in i.Attributes)
                {
                    try
                    {
                        a.Execute(me, i.Property);
                    }
                    catch (AutoAttributeException e)
                    {
                        me.W("{3}\nAuto attribute [{0}] on field {1} in object {2} failed.", a.GetType().Name, i.Property.Name, me.ToString(), e.Message);
                        exceptionCount++;
                    }
                    catch (Exception e)
                    {
                        me.E(e, "Exception while processing attribute [{0}] on class {1}.", a.GetType().Name, me.ToString());
                        exceptionCount++;
                    }
                }
            }

            // If any errors occured, pause the Editor, as they will likely cause many
            //  more "red herring" exceptions once the game starts, and it's better to
            //  tackle the Auto warnings first!
            if (exceptionCount > 0)
                Debug.Break();
        }

        static IEnumerable<BootstrapAttribute> GetAttributes(MemberInfo memberInfo)
        {
            return memberInfo.GetCustomAttributes(typeof(BootstrapAttribute), false).Cast<BootstrapAttribute>();
        }

    }

}
