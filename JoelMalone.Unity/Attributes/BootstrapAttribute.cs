﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    public abstract class BootstrapAttribute : Attribute
    {

        public abstract void Execute(object obj, MemberInfo memberInfo);

    }

}
