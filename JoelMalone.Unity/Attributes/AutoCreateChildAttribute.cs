﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoCreateChildAttribute : AutoComponentBaseAttribute
    {

        public string Name { get; set; }

        protected override Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType)
        {
            string name = Name ?? memberInfo.Name;

            GameObject newGameObject;
            // A bit of a hack to allow specifying Transform when no particular component
            //  type is required, e.g. we just want an empty GameObject, but need to work
            //  around the componet-centric nature of AutoComponentBaseAttribute
            if (searchingType == typeof(Transform))
                newGameObject = new GameObject(name);
            else
                newGameObject = new GameObject(name, searchingType);
            
            newGameObject.transform.parent = searchingComponent.transform;
            newGameObject.transform.localPosition = Vector3.zero;
            var newComponent = newGameObject.GetComponent(searchingType);

            return new Component[] { newComponent };
        }

    }

}
