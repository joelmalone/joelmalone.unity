﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoFromNamedChildAttribute : AutoComponentBaseAttribute
    {

        public string Name;

        protected override Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType)
        {
            var childTransform = searchingComponent.transform.Find(Name);
            if (childTransform == null)
                throw new AutoAttributeException("Unable to find child GameObject with name {0} for component {1}.", Name, searchingComponent);

            return childTransform.GetComponents(searchingType);
        }
    }

}
