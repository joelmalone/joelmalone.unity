﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity.Attributes
{
    
    class AutoAttributeException : Exception
    {

        public AutoAttributeException(string message)
            : base(message)
        { }

        public AutoAttributeException(string format, params object[] args)
            : base(string.Format(format, args))
        { }

    }

}
