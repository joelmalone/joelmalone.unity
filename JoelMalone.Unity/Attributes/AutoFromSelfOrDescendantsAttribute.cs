﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoFromSelfOrDescendantsAttribute : AutoComponentBaseAttribute
    {

        protected override Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType)
        {
            var result = new List<Component>();

            GatherMatchingComponentsRecursively(searchingComponent, searchingType, result);

            return result.ToArray();
        }

        private void GatherMatchingComponentsRecursively(Component component, Type type, IList<Component> target)
        {
            var matching = component.GetComponents(type);
            foreach (var match in matching)
                target.Add(match);

            for(int i=0; i<component.transform.childCount; i++)
            {
                var child = component.transform.GetChild(i);
                GatherMatchingComponentsRecursively(child, type, target);
            }
        }

    }

}
