﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoAttribute : AutoComponentBaseAttribute
    {

        protected override Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType)
        {
            return searchingComponent.GetComponents(searchingType);
        }

    }

}
