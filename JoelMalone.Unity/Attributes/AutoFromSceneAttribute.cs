﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity.Attributes
{
    
    [AttributeUsage(AttributeTargets.Field)]
    public class AutoFromSceneAttribute : AutoComponentBaseAttribute
    {

        protected override Component[] ExecuteSearch(Component searchingComponent, MemberInfo memberInfo, Type searchingType)
        {
            // Note: this searches GameObjects, but does not include them
            //  in the set of possible results (can update if necessary)
            var matches = GameObject.FindObjectsOfType(typeof(GameObject))
                .Cast<GameObject>()
                .SelectMany(c => c.GetComponents<Component>())
                .Where(c => c != null && searchingType.IsAssignableFrom(c.GetType()));

            return matches.ToArray();
        }

    }

}
