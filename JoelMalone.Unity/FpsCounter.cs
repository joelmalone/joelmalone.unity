﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Unity
{

    public class FpsCounter
    {
        private readonly float _realtimeUpdateInterval;

        private float _realtimeAccumulated = 0; // FPS accumulated over the interval
        private int _framesThisInterval = 0; // Frames drawn over the interval
        private float _realtimeIntervalRemaining; // Left time for current interval

        private float _fps = 0;

        public FpsCounter()
            : this(0.5f)
        { }

        public FpsCounter(float updateInterval)
        {
            _realtimeUpdateInterval = updateInterval;
            _realtimeIntervalRemaining = updateInterval;
        }

        public void IncrementFrame(float realtimeDelta)
        {
            _realtimeIntervalRemaining -= realtimeDelta;
            _realtimeAccumulated += 1 / realtimeDelta;
            _framesThisInterval++;

            // Interval ended - update GUI text and start new interval
            if (_realtimeIntervalRemaining <= 0.0)
            {
                // Compute the FPS
                _fps = _realtimeAccumulated / _framesThisInterval;

                // Reset counters
                _realtimeAccumulated = 0.0F;
                _framesThisInterval = 0;

                _realtimeIntervalRemaining = _realtimeUpdateInterval;
            }
        }

        public float Fps { get { return _fps; } }

    }

}
