﻿using System;
using System.Diagnostics;
using JoelMalone.Logging;

namespace JoelMalone.Unity
{

    public class StopwatchLog : IDisposable
    {

        public readonly Stopwatch _sw = new Stopwatch();
        public readonly string _tag;
        private readonly object _context;
        private long _limit;

        public StopwatchLog(object context, string tag, long limitMsec)
        {
            _context = context;
            _tag = tag;
            _limit = limitMsec;

            _sw.Start();
        }

        void IDisposable.Dispose()
        {
            var elapsedMsec = _sw.ElapsedMilliseconds;
            if (elapsedMsec > _limit)
            {
                Log.Write(_context, LogLevel.Warning, "Stopwatch \"{0}\" elapsed {1} msec, limit was {2}.", _tag, elapsedMsec, _limit);
            }
            else
            {
                Log.Write(_context, LogLevel.Normal, "Stopwatch \"{0}\" elapsed {1} msec.", _tag, elapsedMsec);
            }
        }
    }

}
