﻿using JoelMalone.Logging;
using JoelMalone.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JoelMalone.Unity
{

    public class UnityLog : Log
    {

        protected override void OnWrite(object context, LogLevel level, string text)
        {
            var flags = BuildFlagsString(context as Component);

            var toString = context == null ? "" : context.ToString();
            text = string.Format("{0}\n        {1}{2}", text, flags, toString);

            switch (level)
            {
                case LogLevel.Critical:
                    UnityEngine.Debug.LogError(text, context as UnityEngine.Object);
                    break;
                case LogLevel.Warning:
                    UnityEngine.Debug.LogWarning(text, context as UnityEngine.Object);
                    break;
                default:
                    UnityEngine.Debug.Log(text, context as UnityEngine.Object);
                    break;
            }
        }

        public static string BuildFlagsString(Component component)
        {
            var flags = string.Empty;
            var networkView = component == null ? null : component.GetComponent<NetworkView>();

            if (Network.isClient)
                flags += "C";
            if (Network.isServer)
                flags += "S";
            if (networkView && networkView.isMine)
                flags += "M";

            if(flags.Length > 0)
                return "[" + flags + "] ";
            else
                return flags;
        }
    }

}