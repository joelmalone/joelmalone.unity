﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using JoelMalone.Logging;

namespace JoelMalone.Unity.AssemblyScanner
{
    public class ScannableAttribute : Attribute
    {

        public static IEnumerable<T> InstantiateAllOfType<T>(Assembly assembly)
        {
            var filterType = typeof(T);
            var implementingTypes = from t in Assembly.GetExecutingAssembly().GetTypes()
                                    where filterType.IsAssignableFrom(t)
                                    && !t.IsAbstract
                                    && t.GetCustomAttributes(typeof(ScannableAttribute), false).Any()
                                    select t;

            assembly.D("Found filters: {0}.", string.Join(", ", implementingTypes.Select(t => t.Name).ToArray()));

            var instances = from t in implementingTypes
                            let ctor = t.GetConstructor(Type.EmptyTypes)
                            select (T)ctor.Invoke(null);

            return instances;
        }

    }
}
